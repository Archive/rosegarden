
clean:	utilities-idl-clean
utilities-idl-clean:
	rm -f utilities/idl/*.[Ch]

UTILITIES_GENHEADERS	= $(patsubst %.idl,%.h,$(shell ls utilities/idl/*.idl))
GENHEADERS	+= $(UTILITIES_GENHEADERS)

utilities/idl/+objects/Make.gen: $(UTILITIES_GENHEADERS)
