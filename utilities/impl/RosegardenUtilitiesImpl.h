// -*- c-file-style:  "bsd" -*-
#ifndef _ROSEGARDEN_UTILITY_IMPL_H_
#define _ROSEGARDEN_UTILITY_IMPL_H_

#include <map>
#include <utility>
#include <utilities/idl/RosegardenUtilities.h>
#include <common/my_wstring.h>

// It's quite acceptable for a client to include this header
// directly, use the type locally, and link against -lutility_impl
// (in contrast with other impl/ directories).  That's why the
// namespace declaration is here

namespace Rosegarden {

class UtilitiesImpl : public Utilities_skel
{
public:
    UtilitiesImpl();
    virtual ~UtilitiesImpl();

    // General conversions

    virtual Time noteLength(NoteType type, CORBA::Boolean dotted);
    virtual void timeToNearestNoteLength(Time t, NoteType &type,
					 CORBA::Boolean &dotted);

    virtual Time timeSignatureToBeatLength(const TimeSignature &ts);
    virtual Time timeSignatureToBarLength(const TimeSignature &ts);

    virtual TimeList *gapToSuitableNoteLengthList(Time start, Time duration,
						  const TimeSignature &ts);

    virtual void keyToSharpsAndFlats
    (Key k, CORBA::Short &sharps, CORBA::Short &flats);
    virtual Key sharpsAndFlatsToKey(CORBA::Short sharps, CORBA::Short flats);

    // MIDI conversions

    virtual Time midiTimeToTime(CORBA::Long midiTime,
				CORBA::Short midiTimeDivision);
    virtual CORBA::Long timeToMidiTime(Time time,
				       CORBA::Short midiTimeDivision);

    virtual Pitch midiPitchToPitch(CORBA::Short midiPitch);
    virtual CORBA::Short pitchToMidiPitch(Pitch p);

    virtual Velocity midiVelocityToVelocity(CORBA::Short midiVelocity);
    virtual CORBA::Short velocityToMidiVelocity(Velocity v);

    // Rosegarden 2.1 conversions

    virtual Time rose21NoteNameToTime(const CORBA::WChar *noteName,
				      CORBA::Boolean dotted);
    virtual CORBA::WChar *timeToRose21NoteName(Time t);

    virtual Pitch rose21PitchToPitch(CORBA::Short r21Pitch,
				     Clef clef, Accidental accidental);
    virtual CORBA::Short pitchToRose21Pitch(Pitch p, //!!! not written yet
					    Clef clef, Accidental &accidental);

    virtual Clef rose21ClefNameToClef(const CORBA::WChar *clefName);
    virtual CORBA::WChar *clefToRose21ClefName(Clef c);

    virtual Key rose21KeyNameToKey(const CORBA::WChar *keyName);
    virtual CORBA::WChar *keyToRose21KeyName(Key c);

private:
    typedef pair<bool, short> SharpCount;
    typedef pair<my_wstring, SharpCount> KeyRec;
    typedef map<Key, KeyRec> KeyRecMap;
    KeyRecMap m_keyMaps;

    struct NoteRec {
	NoteRec(NoteType t, bool d, my_wstring n, Time l) :
	    type(t), dotted(d), name(n), length(l) { }
	NoteType type; bool dotted; my_wstring name; Time length;
    };

    vector<NoteRec *> m_noteTypes;

    void makeTimeListSub(Time t, bool dottedTime, vector<Time> &);
};

};

#endif
