
Things I think are useful
------------------------

 * Tooltips on menu entries and toolbar buttons.  You've got
   to be able to switch them off, though.

 * Status-line comments for any operations that are procedural
   in aspect.  In other words if there's a usual "next step" to
   take at any point, it should tell you so.  Again, expert
   mode should disable this.

 * Self-descriptive menu and dialog text.  Particularly
   dialogs.  Rosegarden 2.1's dialogs are much too terse;
   this is something I feel Windows 95 has the right idea
   about.  A dialog should pose a question, and the buttons
   should answer it.  And there must *always* be a Cancel.

 * A help button (that works) on every dialog.  Rosegarden 2.1
   tries but fails a bit, because the Help buttons on e.g. file
   dialogs just give you generic help about using a file dialog
   -- which means a menu function that only invokes a file
   dialog has no specific help available.  Ideally you'd get
   help about using a file dialog, with a link back to the help
   page for whichever function you invoked the dialog from.

 * A search function in the Help.  Even Rosegarden 2.1's rather
   paltry keyword-search function was better than nothing.

 * Usage FAQ online.  Windows 95 apps tend to do something
   similar, with the Office-style "What do you want to do?
   --format a table? --export a table into an HTML document?
   --some other fictitious thing I've just thought up?"
   I don't like this presentation, but I think there are things
   to be learned by reading help in Q/A format.

 * Tip of the day!  I've surprised myself by finding I actually
   quite like the tip of the day you get on startup from the Gimp.
   (btw, I think some aspects of the Gimp are awful -- particularly
   the dialogs.)

   In general I don't like the tips Word gives you (nor those from
   e.g. Windows NT on bootup) because they're aimed rather too
   squarely at beginners -- often by the time you've run the program
   enough times to have seen more than a few tips, you'll already know
   what they're talking about.  But the Gimp's tips are mostly rather
   more advanced, and lead me to want to investigate features I may
   not have known existed in the first place.  A fairly random
   mixture, starting with a couple of simple ones, would perhaps be
   nice.

None of these are particularly thrilling or high-tech features.

Things I don't find very useful
-----------------------------

 * Tutorials online, although a Minimal Tutorial on paper is nice

 * "What's this?" and click-for-context help -- things like the
   question-mark buttons on Win95 stuff.  It's rare that I want
   more help on a specific interface element than a tooltip can
   give; more likely I'll want help on the function or errors
   that arise from using it, and click-for-context doesn't often
   help much with that.


_Chris Cannam_
