
List classes in use in Rosegarden
=================================

The Rosegarden code uses several different classes to store simple
sequences of elements.

 1. STL [vector].  This has the advantage of simplicity,
    and is useful if you want a list that doesn't have to be
    extensively edited.   Fast to create and access but
    generally slow to edit, particularly if large.  If you're
    writing new code, consider these for the first pass.

    Of course, the STL also offers several other classes to
    store data non-sequentially, which may be more appropriate.

 2. Our own [FastInsertVector].  This class (in [common/]) has
    an interface designed to be fairly close to the STL [vector]
    class, and can normally be slotted in as a direct
    replacement.  However, it favours speed over correctness
    and doesn't preserve the correct semantics for element
    copy.  This is only a problem if you choose to store
    objects that maintain internal pointers -- for instance,
    an object that contains a delegation pointer to an
    inner object, that itself points back to the outer object;
    the address of the outer object may change without
    warning, breaking any such back-pointer.  This is only
    a problem if you store objects: a [FastInsertVector] of
    pointers to objects (or [CloningWrapper]s, below) has
    no such limitation.  See <URL:fast-insert-vector.txt> for
    a description of these lists, in which I attempt to justify
    them on the grounds of efficiencies much smaller than those
    lost through using CORBA anyway.  Oh well.

 3. Our own [RobustIteratorList].  This (also in [common]/)
    is an abstract container, in that you can specify [vector],
    [FastInsertVector] or some other random-access sequence
    as the storage type.  This class provides an interface
    similar to the STL [vector] and its iterators, except
    that it attempts to ensure that modifications made with
    one iterator don't affect the location of other iterators
    -- they stay pointing to the same element, if possible, or
    at least to *some* element. This is the basis of the
    Rosegarden server's ability to serve multiple applications
    concurrently.

    Note that the iterator semantics of [RobustIteratorList]
    are sometimes counterintuitive.  For example, if you create
    two iterators at the list's [end()] and use one of them to
    insert repeatedly, the other iterator will move right
    as you insert and will remain at the end of the list -- it
    won't stay at the start of the insertion as you may expect.

 4. CORBA [sequence]s.  These are generated from IDL specs and
    are always typedef'd to names unique to the particular
    sequence type (i.e. they don't appear as templated classes
    in C++).  They're obviously necessary or useful when working
    with CORBA, but have little application for general use: the
    interface is very limited -- they don't even resize
    automatically -- and you can in any case only use sequences
    that form part of the IDL specification.

 5. Rosegarden 2.1 linked lists.  These are only used in the old
    code in [midilib].  They're written in C, and both interface
    and semantics are distinctly different from any of the newer
    STL-like iterator-based lists.  Avoid, unless you want to
    confuse yourself thoroughly about both types of list.


