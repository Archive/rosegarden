
DOCHTML	:= $(patsubst %.txt,%.html,$(shell ls doc/*.txt doc/[a-z]*/*.txt))
DOCUMENTATION	+= $(DOCHTML) doc/index.html

clean:	clean-doc

clean-doc:
	rm -f $(DOCUMENTATION)

doc/index.html:	$(DOCHTML)
	rm -f doc/index.html ; scripts/htmlindex.sh doc/*.html doc/[a-z]*/*.html > doc/index.html

