// -*- c-file-style:  "bsd" -*-
#ifndef _MY_WSTRING_CXX_
#define _MY_WSTRING_CXX_

#include "my_wstring.h"

#include <vector>
#include <string>
#include <cstdlib>
#include <algorithm>

template <class Char, class Container>
my_string<Char, Container>::my_string(string s)
    : m_b(0)
{
    for_each(s.begin(), s.end(), AppendTo(this));
}

template <class Char, class Container>
my_string<Char, Container>::my_string(const char *s)
    : m_b(0)
{
    for (int i = 0; s[i]; ++i) {
	m_container.push_back(s[i]);
    }
}

template <class Char, class Container>
my_string<Char, Container>::my_string(const Char *w)
    : m_b(0) 
{
    for (int i = 0; w[i]; ++i) {
	m_container.push_back(w[i]);
    }
}

template <class Char, class Container>
my_string<Char, Container>::my_string(const Container &t)
    : m_container(t), m_b(0)
{
    // empty
}

template <class Char, class Container>
my_string<Char, Container>::my_string(const my_string<Char, Container> &mw)
    : m_container(mw.m_container), m_b(0)
{
    // empty
}

template <class Char, class Container>
my_string<Char, Container> &my_string<Char, Container>::operator=(const my_string<Char, Container> &mw)
{
    if (m_b) free(m_b);
    m_b = 0;
    m_container = mw.m_container;
    return *this;
}

template <class Char, class Container>
my_string<Char, Container>::~my_string()
{
    if (m_b) free(m_b);
}

template <class Char, class Container>
my_string<Char, Container>::size_type my_string<Char, Container>::size() const
{
    return m_container.size();
}

template <class Char, class Container>
const Char *my_string<Char, Container>::c_str() const
{
    if (!m_b) m_b = (Char *)malloc((size()+1) * sizeof(Char));
    else m_b = (Char *)realloc(m_b, (size()+1) * sizeof(Char));
    
    unsigned int i;
    for (i = 0; i < size(); ++i) {
	m_b[i] = m_container[i];
    }
    m_b[i] = 0;
    
    return m_b;
}

template <class Char, class Container>
const char *my_string<Char, Container>::narrow_string() const
{
    if (!m_b) m_b = (Char *)malloc((size()+1) * sizeof(char));
    else m_b = (Char *)realloc(m_b, (size()+1) * sizeof(char));
    
    unsigned int i;
    for (i = 0; i < size(); ++i) {
	((char *)m_b)[i] = m_container[i];
    }
    ((char *)m_b)[i] = 0;
    
    cerr << "Warning: narrowing wide string to " << (char *)m_b << endl;
    
    return (char *)m_b;
}

template <class Char, class Container>
bool my_string<Char, Container>::operator==(const my_string<Char, Container> &ws) const
{
    if (size() != ws.size()) return false;
    for (size_type i = 0; i < size(); ++i) {
	if (m_container[i] != ws.m_container[i]) return false;
    }
    return true;
}

template <class Char, class Container>
bool my_string<Char, Container>::operator==(const char *s) const
{
    size_type i;
    for (i = 0; i < size(); ++i) {
	if (!s[i]) return false;
	if (m_container[i] != Char(s[i])) return false;
    }
    if (s[i]) return false;
    else return true;
}

template <class Char, class Container>
Char my_string<Char, Container>::operator[](my_string<Char, Container>::size_type i) const
{
    return m_container[i];
}

template <class Char, class Container>
Char &my_string<Char, Container>::operator[](my_string<Char, Container>::size_type i)
{
    return m_container[i];
}

template <class Char, class Container>
void my_string<Char, Container>::AppendTo::operator()(const char &c)
{
    m_s->m_container.push_back(Char(c));
}

#endif
