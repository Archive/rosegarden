// -*- c-file-style:  "bsd" -*-
#ifndef _ROBUST_ITERATOR_LIST_CXX_
#define _ROBUST_ITERATOR_LIST_CXX_

#include "RobustIteratorList.h"
#include <algorithm>

// Iterator

template <class T, class Container>
RobustIteratorList<T, Container>::iterator::iterator
(RobustIteratorList<T, Container> *const ril, size_type position)
    : m_list(ril), m_position(position)
{
    m_list->registerIterator(this);
}

template <class T, class Container>
RobustIteratorList<T, Container>::iterator::iterator
(const RobustIteratorList<T, Container>::iterator &i)
    : m_list(i.m_list), m_position(i.m_position)
{
    m_list->registerIterator(this);
}

template <class T, class Container>
RobustIteratorList<T, Container>::iterator::~iterator()
{
    m_list->unregisterIterator(this);
}

template <class T, class Container>
RobustIteratorList<T, Container>::iterator &
RobustIteratorList<T, Container>::iterator::operator=
(const RobustIteratorList<T, Container>::iterator &i)
{
    if (&i != this) {
	m_position = i.m_position;
	if (m_list != i.m_list) {
	    m_list->unregisterIterator(this);
	    m_list = i.m_list;
	    m_list->registerIterator(this);
	}
    }
    return *this;
}

// Container

template <class T, class Container>
RobustIteratorList<T, Container>::~RobustIteratorList()
{
    if (m_iterators.size() != 0) {
 	cerr << "ERROR: ~RobustIteratorList with " << m_iterators.size() << " iterators still extant" << endl;
    }
    assert(m_iterators.size() == 0);
}

template <class T, class Container>
RobustIteratorList<T, Container> &RobustIteratorList<T, Container>::operator=
(const RobustIteratorList<T, Container> &ril)
{
    if (this != &ril) {
	m_container = ril.m_container;
	for_each(m_iterators.begin(), m_iterators.end(), GoTo(begin()));
    }
    return *this;
}

template <class T, class Container>
void RobustIteratorList<T, Container>::erase
(const RobustIteratorList<T, Container>::iterator &i)
{
    int ip(i.m_position);
    m_container.erase(i.containerIterator());
    for_each(m_iterators.begin(), m_iterators.end(),
		 NotifyErase(this, ip));
}

template <class T, class Container>
void RobustIteratorList<T, Container>::erase
(const RobustIteratorList<T, Container>::iterator &i,
 const RobustIteratorList<T, Container>::iterator &j)
{
    //!!! is this right? I guess not size_type
    int ip(i.m_position);
    size_type n = ::std::distance(i, j);
    m_container.erase(i.containerIterator(), j.containerIterator());
    for_each(m_iterators.begin(), m_iterators.end(),
		 NotifyErase(this, ip, n));
}

template <class T, class Container>
RobustIteratorList<T, Container>::iterator RobustIteratorList<T, Container>::insert
(const RobustIteratorList<T, Container>::iterator &i, const T &t)
{
    int ip(i.m_position);
    m_container.insert(i.containerIterator(), t);
    for_each(m_iterators.begin(), m_iterators.end(),
		 NotifyInsert(this, ip, 1));
    return iterator(this, ip);
}

#endif

