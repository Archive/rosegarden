// -*- c-file-style:  "bsd" -*-
#ifndef _ROBUST_ITERATOR_LIST_
#define _ROBUST_ITERATOR_LIST_

#include <iostream>
#include <cassert>

#include <set>
#include <vector>
#include <iterator>

// STL-style ADT.  Container must support random-access iterators

template <class T, class Container = ::std::vector<T> >
class RobustIteratorList
{
public:
    typedef typename Container::value_type value_type;
    typedef typename Container::size_type size_type;
    typedef Container container_type;

private:
    // functors
    struct NotifyErase;
    struct NotifyInsert;
    struct GoTo;

public:
    class iterator
#ifdef _STL_1997_
	: public ::std::iterator<::std::random_access_iterator_tag, T, size_type>
#else
        : public 
#ifdef __STL_USE_NAMESPACES
         ::std::
#endif
         random_access_iterator<T, size_type>
#endif
    {
    public:
	iterator(const iterator &i);
	virtual ~iterator();

	iterator &operator=(const iterator &i);

	iterator &operator--() {
	    if (m_position > 0) --m_position;
	    return *this;
	}

	iterator operator--(int) {
	    iterator i(*this);
	    (void)operator--();
	    return i;
	}

	iterator &operator++() {
	    if (m_position < m_list->size()) ++m_position;
	    return *this;
	}

	iterator operator++(int) {
	    iterator i(*this);
	    (void)operator++();
	    return i;
	}

	bool sameListAs(const iterator &i) const {
	    return m_list == i.m_list;
	}

	bool operator==(const iterator &i) const {
	    return m_list == i.m_list && m_position == i.m_position;
	}
	bool operator!=(const iterator &i) const {
	    return m_list != i.m_list || m_position != i.m_position;
	}
	bool operator<(const iterator &i) const {
	    assert(m_list == i.m_list);
	    return m_position < i.m_position;
	}

	size_type operator-(const iterator &i) const {
	    return m_position - i.m_position;
	}
	size_type operator+(const iterator &i) const {
	    return m_position + i.m_position;
	}
	iterator &operator-=(const iterator &i) {
	    m_position -= i.m_position;
	    if (m_position < 0) m_position = 0;
	    return *this;
	}
	iterator &operator+=(const iterator &i) {
	    m_position += i.m_position;
	    if (m_position > m_list->size()) m_position = m_list->size();
	    return *this;
	}
	iterator &operator+=(size_type n) {
	    m_position += n;
	    if (m_position > m_list->size()) m_position = m_list->size();
	    return *this;
	}

	T &operator*() {
	    return *containerIterator();
	}

	const T &operator*() const {
	    return *containerIterator();
	}

	T *operator->() {
	    return &(operator*());
	}

	const T *operator->() const {
	    return &(operator*());
	}

    protected:
	friend class RobustIteratorList<T, Container>;

	// list functors allowed access
	friend struct NotifyErase;
	friend struct NotifyInsert;
	friend struct GoTo;

	iterator(RobustIteratorList<T, Container> *const ril, size_type position);

	void listElementsAdded(RobustIteratorList<T, Container> *const ril,
	    size_type i, size_type n) {
	    assert(m_list == ril);
	    if (i <= m_position) m_position += n;
	}

	void listElementsRemoved(RobustIteratorList<T, Container> *const ril,
	    size_type i, size_type n) {
	    assert(m_list == ril);
	    if (i < m_position) m_position -= n;
	}

	typename Container::iterator containerIterator() {
	    typename Container::iterator i(m_list->m_container.begin());
	    i += m_position;
	    return i;
	}

	const typename Container::iterator containerIterator() const {
	    typename Container::iterator i(m_list->m_container.begin());
	    i += m_position;
	    return i;
	}

	RobustIteratorList<T, Container> *m_list;
	size_type m_position;
    };

    explicit RobustIteratorList(const Container &container = Container()) :
	m_container(container) { }

    RobustIteratorList(const RobustIteratorList<T, Container> &l) :
	m_container(l.m_container) { }

    template <class InputIterator>
    RobustIteratorList(InputIterator first, InputIterator last) {
	while (first != last) {
	    m_container.push_back(*first);
	    ++first;
	}
    }

    virtual ~RobustIteratorList();

    RobustIteratorList<T, Container> &operator=(const RobustIteratorList<T, Container> &ril);

    size_type size() const {
	return m_container.size();
    }

    bool empty() const {
	return m_container.empty();
    }

    void erase(const iterator &i);
    void erase(const iterator &i, const iterator &j);

    // Returns an iterator pointing at the start of the newly-inserted
    // item, in common with the STL containers (but the iterator "i"
    // will be moved along to the right, to point to the end of the
    // new item)
    iterator insert(const iterator &i, const T &t);

    void push_back(const T &t) {
	(void) insert(end(), t);
    }

    iterator begin() const {
	return iterator((RobustIteratorList<T, Container> *)this, 0);
    }

    iterator end() const {
	return iterator((RobustIteratorList<T, Container> *)this, size());
    }

    // this is a temporary thing for debugging only
    void dump(ostream &ostr) {
	for (int i = 0; i < m_container.size(); ++i) {
	    ostr << i << ": " << &m_container[i] << endl;
	}
    }

private:
    friend class iterator;
 
    struct NotifyErase // functor
    {
	NotifyErase(RobustIteratorList<T, Container> *ril, size_type i, size_type n = 1) :
	    m_list(ril), m_i(i), m_n(n) { }
	void operator()(iterator *ii) {
	    ii->listElementsRemoved(m_list, m_i, m_n);
	}
	RobustIteratorList<T, Container> *m_list;
	size_type m_i, m_n;
    };

    struct NotifyInsert // functor
    {
	NotifyInsert(RobustIteratorList<T, Container> *ril, size_type i, size_type n = 1) :
	    m_list(ril), m_i(i), m_n(n) { }
	void operator()(iterator *ii) {
	    ii->listElementsAdded(m_list, m_i, m_n);
	}
	RobustIteratorList<T, Container> *m_list;
	size_type m_i, m_n;
    };

    struct GoTo // functor
    {
	GoTo(iterator it) : m_it(it) { }
	void operator()(iterator *ii) { *ii = m_it; }
	iterator m_it;
    };

    void registerIterator(iterator *i) {
	m_iterators.insert(i);
    }

    void unregisterIterator(iterator *i) {
	m_iterators.erase(i);
    }

    ::std::set<iterator *> m_iterators;
    Container m_container;
};

#endif

// For gcc-2.7.2 this was a terrible inefficiency, but newer compilers
// seem to expect it:
#include "RobustIteratorList.cxx"

