// -*- c-file-style:  "bsd" -*-
#ifndef _FAST_INSERT_VECTOR_H_
#define _FAST_INSERT_VECTOR_H_

#include <iterator>

template <class T>
class FastInsertVector
{ 
public:
    typedef T value_type;
    typedef signed long size_type;

    class iterator
#ifdef _STL_1997_
	: public ::std::iterator<::std::random_access_iterator_tag, T, size_type>
#else
        : public 
#ifdef __STL_USE_NAMESPACES
         ::std::
#endif
         random_access_iterator<T, size_type>
#endif
    {
    public:
	iterator() : m_v(0), m_i(-1) { }
	iterator(const iterator &i) : m_v(i.m_v), m_i(i.m_i) { }
	virtual ~iterator() { }

	iterator &operator=(const iterator &i) {
	    if (&i != this) { m_v = i.m_v; m_i = i.m_i; }
	    return *this;
	}

	iterator &operator--() { --m_i; return *this; }
	iterator operator--(int) { iterator i(*this); --m_i; return i; }
	iterator &operator++() { ++m_i; return *this; }
	iterator operator++(int) { iterator i(*this); ++m_i; return i; }

	bool operator==(const iterator &i) const {
	    return (m_v == i.m_v && m_i == i.m_i);
	}

	bool operator!=(const iterator &i) const {
	    return (m_v != i.m_v || m_i != i.m_i);
	}

	iterator &operator+=(size_type i) { m_i += i; return *this; }
	iterator &operator-=(size_type i) { m_i -= i; return *this; }

	T &operator*() { return m_v->at(m_i); }
	const T &operator*() const { return m_v->at(m_i); }

	T *operator->() { return &(operator*()); }
	const T *operator->() const { return &(operator*()); }
	
    private:
	friend FastInsertVector<T>;
	iterator(FastInsertVector<T> *v, size_type i) : m_v(v), m_i(i) { }
	FastInsertVector<T> *m_v;
	size_type m_i;
    };

public: 
    FastInsertVector() :
	m_items(0), m_count(0), m_gapStart(-1),
	m_gapLength(0), m_size(0) { }
    FastInsertVector(const FastInsertVector<T> &);
    virtual ~FastInsertVector();

    FastInsertVector<T> &operator=(const FastInsertVector<T> &);

    iterator begin() { return iterator(this, 0); }
    iterator end() { return iterator(this, m_count-1); }
    size_type size() const { return m_count; }
    bool empty() const { return m_count == 0; }

    // not all of these are defined yet
    void swap(FastInsertVector<T> &v);
    bool operator==(const FastInsertVector<T> &) const;
    bool operator!=(const FastInsertVector<T> &v) const { return !operator==(v); }
    bool operator<(const FastInsertVector<T> &) const;
    bool operator>(const FastInsertVector<T> &) const;
    bool operator<=(const FastInsertVector<T> &) const;
    bool operator>=(const FastInsertVector<T> &) const;

    T& at(size_type index) {
	assert(index >= 0 && index < m_count);
	return m_items[externalToInternal(index)];
    }
    const T& at(size_type index) const {
	return ((FastInsertVector<T> *)this)->at(index);
    }
 
    T &operator[](size_type index) {
	return at(index);
    }
    const T &operator[](size_type index) const {
	return at(index);
    }

    virtual T* array(size_type index, size_type count); 

    // We *guarantee* that prepend and append modify the FastInsertVector only
    // through a call to insert.  This is important because subclasses
    // only need to override one function to catch all mutations
    virtual void push_front(const T& item) { insert(0, item); }
    virtual void push_back(const T& item) { insert(m_count, item); }

    // basic insert -- all others call this
    virtual void insert(size_type index, const T&);

    virtual iterator insert(const iterator &p, const T &t) {
	insert(p.m_i, t);
	return p;
    }

// not yet
//    virtual iterator insert(iterator &p, size_type index, const T &);

    template <class InputIterator>
    iterator insert(iterator &p, InputIterator &i, InputIterator &j);
    
    virtual iterator erase(const iterator &i) {
	assert(i.m_v == this);
	remove(i.m_i);
	return iterator(this, i.m_i);
    }

    virtual iterator erase(const iterator &i, const iterator &j);
    virtual void clear();

private:
    // basic remove -- erase(), clear() call this
    virtual void remove(size_type index); 

    void resize(size_type needed); // needed is internal (i.e. including gap)

    void moveGapTo(size_type index);	// index is external
    void closeGap() {
	if (m_gapStart >= 0) moveGapTo(m_count);
	m_gapStart = -1;
    }

    size_type bestNewCount(size_type n, size_t s) const {
	if (m_size == 0) {
	    if (n < 8) return 8;
	    else return n;
	} else {
	    // double up each time -- it's faster than just incrementing
	    size_type s(m_size);
	    if (s > n*2) return s/2;
	    while (s <= n) s *= 2;
	    return s;
	}
    }

    size_type externalToInternal(size_type index) const {
	return ((index < m_gapStart || m_gapStart < 0) ?
		index : index + m_gapLength);
    } 

    size_type minSize() const { return 8; }
    size_t minBlock() const {
	return minSize() * sizeof(T) > 64 ? minSize() * sizeof(T) : 64;
    }

    T* m_items;
    size_type m_count;		// not counting gap
    size_type m_gapStart;		// -1 for no gap
    size_type m_gapLength;		// undefined if no gap
    size_type m_size;
};  

template <class T> void *operator new(size_t size, FastInsertVector<T> *list,
				      void *space);

#endif

// For gcc-2.7.2 this was a terrible inefficiency, but newer compilers
// seem to expect it:
#include "FastInsertVector.cxx"

