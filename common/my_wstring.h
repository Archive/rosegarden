// -*- c-file-style:  "bsd" -*-
#ifndef _my_wstring_h_
#define _my_wstring_h_

#include <string>
#include <vector>

template <class Char, class Container> class my_string;
template <class Char, class Container>
const char *narrow_my_string(const my_string<Char, Container> &mws);

// Named as a reminder that this is just a placeholder for
// the proper STL class.

template <class Char, class Container = vector<Char> >
class my_string
{
public:
    my_string(string s = "");
    my_string(const Char *w);

    // this shouldn't really be here (it'll screw up Char==char,
    // for a start) but I can't be bothered to do it right in this
    // ten-minute hack
    my_string(const char *s);

    my_string(const Container &t);
    my_string(const my_string<Char, Container> &mw);
    my_string &operator=(const my_string &mw);
    virtual ~my_string();

    typedef typename Container::size_type size_type;

    size_type size() const;
    const Char *c_str() const;

    bool operator==(const my_string<Char, Container> &ws) const;
    bool operator==(const char *s) const;

    Char operator[](size_type i) const;
    Char &operator[](size_type i);

private:
    Container m_container;
    mutable Char *m_b;

    friend struct AppendTo {
	AppendTo(my_string *s) : m_s(s) { }
	void operator()(const char &c);
	my_string *m_s;
    };

    const char *narrow_string() const;
    friend const char *narrow_my_string<Char, Container>
        (const my_string<Char, Container> &);
};

template <class Char, class Container>
ostream &operator<<(ostream &o, const my_string<Char, Container> &ms)
{
    for (my_string<Char, Container>::size_type i = 0; i < ms.size(); ++i) {
	if (ms[i] <= 0xff) {
	    o << (char)ms[i];
	} else {
	    //!!! This is almost certainly wrong.
	    o << (char)(ms[i] >> 8) << (char)(ms[i] & 0xff);
	}
    }
    return o;
}

template <class Char, class Container>
const char *narrow_my_string(const my_string<Char, Container> &mws)
{
    return mws.narrow_string();
}

typedef my_string<wchar_t, vector<wchar_t> > my_wstring;

#endif

// For gcc-2.7.2 this was a terrible inefficiency, but newer compilers
// seem to expect it:
#include "my_wstring.cxx"

