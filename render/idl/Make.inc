
clean:	render-idl-clean
render-idl-clean:
	rm -f render/idl/*.[Ch]

RENDER_GENHEADERS	= $(patsubst %.idl,%.h,$(shell ls render/idl/*.idl))
GENHEADERS	+= $(RENDER_GENHEADERS)

render/idl/+objects/Make.gen: $(RENDER_GENHEADERS)

