// -*- c-file-style:  "bsd" -*-
#ifndef _BAR_LAYOUT_H_
#define _BAR_LAYOUT_H_

#include <server/idl/RosegardenServer.h>
#include "CompositionLayoutImpl.h"

class BarLayoutImpl;

class BarLayoutEngineImpl : virtual public Rosegarden::Render::BarLayoutEngine_skel
{
public:
    BarLayoutEngineImpl() { }
    virtual ~BarLayoutEngineImpl() { }

    virtual void render(Rosegarden::Render::BarLayout_ptr layout,
			Rosegarden::Server::BarListIterator_ptr itr,
			CORBA::Long x, CORBA::Long width) = 0;

    virtual CORBA::Long getMinimumWidth
    (Rosegarden::Render::BarLayout_ptr layout,
     Rosegarden::Server::BarListIterator_ptr itr) = 0; //!!! consider scale
};


class BarLayoutImpl : virtual public Rosegarden::Render::BarLayout_skel
{
public:
    BarLayoutImpl(Rosegarden::Render::CompositionLayout_ptr composition,
		  Rosegarden::Render::BarLayoutEngine_ptr engine,    // I don't own this, it's probably a singleton somewhere
		  Rosegarden::Server::BarListIterator_ptr itr) : // and this
	m_composition(composition),
	m_engine(engine),
	m_iterator(itr), m_x(-1) { }
    virtual ~BarLayoutImpl() {
	m_iterator->releaseImpl();
	CORBA::release(m_iterator);
    }

    virtual void releaseImpl() { CORBA::release(this); }

    virtual void changeCallback() { assert(0); } // obviously needs to take some arg

    virtual CORBA::Long getCurrentX() { // from last rendering
	return m_x; //!!! consider scale
    }

    virtual Rosegarden::Time getBarLength() {
	Rosegarden::Server::Part_ptr p;
	Rosegarden::Time t = m_iterator->getMaxBarDuration(p);
	CORBA::release(p);
	return t;
    }
    virtual CORBA::Long getMinimumWidth() {
	return m_engine->getMinimumWidth(this, m_iterator);
    }

    // The implementation is expected to know its own proper
    // y-coord and height, i.e. to know which stave it is (the
    // height depends on the translation of pitch into position,
    // which is not the concern of the CompositionLayout)
    virtual void render(CORBA::Long x, CORBA::Long width) {
	m_x = x;
	m_engine->render(this, m_iterator, x, width);
    }

private:
    Rosegarden::Render::CompositionLayout_ptr m_composition; // owns me
    Rosegarden::Render::BarLayoutEngine_ptr m_engine;
    Rosegarden::Server::BarListIterator_ptr m_iterator;
    CORBA::Long m_x;
};

#endif

