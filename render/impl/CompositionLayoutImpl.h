// -*- c-file-style:  "bsd" -*-
#ifndef _COMPOSITION_LAYOUT_H_
#define _COMPOSITION_LAYOUT_H_

#include <server/idl/RosegardenServer.h>
#include <render/idl/RosegardenRender.h>

class CompositionLayoutImpl;
class BarLayoutEngine;

class CompositionLayoutEngineImpl : virtual public Rosegarden::Render::CompositionLayoutEngine_skel
{
public:
    CompositionLayoutEngineImpl() { }
    virtual ~CompositionLayoutEngineImpl() { }

    virtual void render(Rosegarden::Render::CompositionLayout_ptr layout,
			CORBA::Short firstBar, CORBA::Short lastBar) = 0;
};


class CompositionLayoutImpl : virtual public Rosegarden::Render::CompositionLayout_skel
{
public:
    CompositionLayoutImpl(Rosegarden::Render::CompositionLayoutEngine_ptr cle, // I don't own the engines, they're probably singletons somewhere
			  Rosegarden::Render::BarLayoutEngine_ptr ble,
			  Rosegarden::Server::Composition_ptr composition) :
	m_xscale(45), m_yscale(45), m_compositionEngine(cle),
	m_barEngine(ble), m_composition(composition) {
	m_barList = composition->getBarList();
    }
    virtual ~CompositionLayoutImpl() { }


    // IDL-defined methods

    virtual void setScale(CORBA::Short xscale, CORBA::Short yscale) {
	m_xscale = xscale;
	m_yscale = yscale;
    }

    virtual void getScale(CORBA::Short &xscale, CORBA::Short &yscale) {
	xscale = m_xscale;
	yscale = m_yscale;
    }

    virtual CORBA::Short getXScale() { return m_xscale; }
    virtual CORBA::Short getYScale() { return m_yscale; }

    virtual void changeCallback() { assert(0); } // obviously needs to take some arg

    virtual void render(CORBA::Short firstBar, CORBA::Short lastBar) {
	m_compositionEngine->render(this, firstBar, lastBar);
    }


    // Non-IDL methods -- use CORBA types for consistency, even so

    virtual CORBA::Short getPartCount() {
	return m_composition->getPartCount();
    }

    // Caller owns the returned servant, and so must call
    // releaseImpl() on it to release the servant in addition to
    // ensuring CORBA::release is used to release the client stub
    virtual Rosegarden::Render::BarLayout_ptr getBarLayout
    (CORBA::Short barNo, CORBA::Short partNo);


private: 
    CORBA::Short m_xscale;
    CORBA::Short m_yscale;

    Rosegarden::Render::CompositionLayoutEngine_ptr m_compositionEngine;
    Rosegarden::Render::BarLayoutEngine_ptr m_barEngine;

    Rosegarden::Server::Composition_ptr m_composition;
    Rosegarden::Server::BarList_ptr m_barList;
};


#endif

