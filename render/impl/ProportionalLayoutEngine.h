
// -*- c-file-style:  "bsd" -*-
#ifndef _PROPORTIONAL_SYSTEM_LAYOUT_H_
#define _PROPORTIONAL_SYSTEM_LAYOUT_H_

#include "BarLayoutImpl.h"
#include "CompositionLayoutImpl.h"

// Layout classes for things like piano-roll, where the visible note
// positions and spacings or sizes are directly proportional to their
// times.  You derive from the BarLayout class an implementation
// specific to your client; the nature of the BarLayout storage and
// how bar data is updated when something at the server changes will
// vary from client to client

class ProportionalBarLayoutEngine :
    virtual public BarLayoutEngineImpl
{
public:
    ProportionalBarLayoutEngine() { }
    virtual ~ProportionalBarLayoutEngine();

    virtual void render(Rosegarden::Render::BarLayout_ptr layout,
			Rosegarden::Server::BarListIterator_ptr itr,
			CORBA::Long x, CORBA::Long width);

    // That render method (which may be quite involved) does the
    // x-layout and then calls on other methods to do the y-layout and
    // drawing.  Those methods are defined by the Gnome(etc) subclass,
    // but need to be declared abstract in this class -- once we've
    // decided what they're going to be.  (Probably some
    // "start-rendering" method which passes in a pointer to the
    // layout object associated with this bar; then individual
    // "render-object" methods which pass in elementitr objects; the
    // Gnome class shouldn't store those, but build up its own minimal
    // data structure associated with the bar as it renders.  (So that
    // if called on by the canvas to redisplay without an associated
    // render-request from the server or user, it can redraw without
    // querying the server.))

    // So, the Gnome(etc) class is solely derived from this one.  It
    // contains the data storage, which it builds up as it renders
    // from the data passed to it by the render methods... does it?

    // Anyway, by this stage we should have enough data hanging around
    // to be able to change the design of the Gnome(etc) layer at a
    // later stage if we need to.

    virtual CORBA::Long getMinimumWidth
    (Rosegarden::Render::BarLayout_ptr layout,
     Rosegarden::Server::BarListIterator_ptr itr); //!!! consider scale
};

class ProportionalCompositionLayoutEngine :
    virtual public CompositionLayoutEngineImpl
{
public:
    ProportionalCompositionLayoutEngine() { }
    virtual ~ProportionalCompositionLayoutEngine();

    virtual void render(Rosegarden::Render::CompositionLayout_ptr layout,
			CORBA::Short firstBar, CORBA::Short lastBar);	
};

#endif

