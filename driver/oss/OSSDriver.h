// -*- c-file-style:  "bsd" -*-
#ifndef _OSS_DRIVER_H_
#define _OSS_DRIVER_H_

#include <performer/impl/SoundDriver.h>

class OSSDriver : public SoundDriver
{
public:
  OSSDriver();
  virtual ~OSSDriver();

  virtual void getCapabilities();
  virtual void getInstruments();

private:
};

#endif // _OSS_DRIVER_H_
