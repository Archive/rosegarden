#!/usr/bin/perl -w
# 
# A plain-text documentation processor.
# See txt2html.txt or txt2html.html for documentation.
# 
# Output from this program can be run through tableofcontents.pl
# to add a table of contents.
# 
# Copyright Chris Cannam 1998

use strict;

my @p;
my $laststyle = "none";

my $title = $ARGV[0];
$title =~ s/\.txt//;
print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">';
print "\n<HTML><HEAD><TITLE>$title</TITLE></HEAD><BODY>\n";
print "<!-- Do not edit! Automatically generated from $ARGV[0] by $0 -->\n";
#print "<CONTENTS>\n"; # for automatic TOC generator

my $hadtoc = 0;

# Returned strings here are symbolic -- they only resemble HTML.
# Don't change them, change the interpretation of them in write_para

sub style_para
{
    # heading?
    if ($#p == 1) {
	$p[1] =~ s/^=+$// and return "h1";
	$p[1] =~ s/^-+$// and return "h2";
	$p[1] =~ s/^~+$// and return "h3";
    }

    # something wholly indented?
    my @nni = grep { /^\s/ } @p;
    if (defined @p and $#nni == $#p) {
	$p[0] =~ s/^\s+[^\w\s]\s+([\w\"])/$1/ and return "uli";
	$p[0] =~ s/^\s+\d+[^\w\s]\s+([\w\"])/$1/ and return "oli";
	$p[0] =~ s/^(\s+[^\w\s\d])/$1/ and return "pre";
	return "cont";
    }
    
    # anything else
    return "p";
}

sub write_para
{
    my $style = shift;

    if ($style eq "cont" and !($laststyle eq "uli" or $laststyle eq "oli")) {
	$style = "pre";
    }

    if (!($style eq $laststyle or $style eq "cont")) {

	if ($laststyle eq "uli") {
	    print "</UL>";
	} elsif ($laststyle eq "oli") {
	    print "</OL>";
	} elsif ($laststyle eq "pre") {
	    print "</PRE>";
	}

	if ($style eq "uli") {
	    print "<UL>";
	} elsif ($style eq "oli") {
	    print "<OL>";
	} elsif ($style eq "pre") {
	    print "<PRE>";
	}
    }

    print "\n";

    if (!$hadtoc and $style ne "h1") {
        print "<CONTENTS>\n";
        $hadtoc = 1;
    }

    if ($style eq "h1") {
	chop $p[0];
	print "<H1>$p[0]</H1>\n";
    } elsif ($style eq "h2") {
	chop $p[0];
	print "<H2>$p[0]</H2>\n";
    } elsif ($style eq "h3") {
	chop $p[0];
	print "<H3>$p[0]</H3>\n";
    } elsif ($style eq "uli" or $style eq "oli") {
	print "</LI><P>" if ($laststyle eq $style);
	$p[0] =~ s/^\s+(?:\d+)?[^\w\s]\s+//;
	print "<LI>@p";
    } elsif ($style eq "cont") {
	$style = $laststyle;
	print "<P>@p";
    } elsif ($style eq "p") {
	print "<P>@p</P>";
    } else {
	print @p;
    }

    if (!$hadtoc and $style eq "h1") {
        print "<CONTENTS>\n";
        $hadtoc = 1;
    }

    $laststyle = $style;
}

while (<>)
{
    s/&/&amp;/g;
    s/</&lt;/g;
    s/>/&gt;/g;

    s, &lt; URL: ( [^&:\"]* ) .txt &gt; ,<A HREF="$1.html">$1.html</A>,xg;
    s, &lt; URL: ( [^&\"]*  )      &gt; ,<A HREF="$1">$1</A>,xg;

    s,    \*( \w[\w\s0-9-]* )\*    ,<B>$1</B>,xg;
    s, \b  _( \w[\w\s0-9-]* )_  \b ,<I>$1</I>,xg;

    s, \\([\[\]]) ,$1,xg or s, \[( .*? )\] ,<CODE>$1</CODE>,xg;

    if (/^\s*$/) {
	write_para style_para if $#p >= 0;
	undef @p;
    } else {
	push @p, $_;
	if (/^[=~-]+$/ && $#p == 1) {
	    write_para style_para;
	    undef @p;
	}
    }
}

#write_para style_para if defined @p;
write_para style_para;
print "\n</CONTENTS>\n</BODY>\n</HTML>\n";


