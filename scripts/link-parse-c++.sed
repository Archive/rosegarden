
# Sed script to parse the output from a linker.  This takes the
# linker's output as its input; it should write the names of undefined
# template classes (e.g. List<int>) into the file "templates", and
# leave other undefineds on the output.
#
# If this script produces any stdout at all, the calling process will
# assume there is an undefined that is not a template and fail.
# 
# This version is supposed to handle output from gcc/GNU-ld.
#
# Chris Cannam, April 1998

# Hold lines such as "file.o: In function some_function():":
/: [A-Z].*:$/h

# Lines with undefined <...> stuff go into the templates, and we start again:
/: undefined .*<[^:]*>::/b tmpl
/: undefined .*<[^:]*> virtual/b tmpl
/: undefined .*<[^:]*> type_info/b tmpl

# Other undefineds go into the non-templates, with the held line for context:
/: undefined /{
H
x
n
}

# Anything else is just kinda slow and dull, not really our scene:
d

:tmpl
s/^.*: undefined .*`\([^:]*>\)::.*$/\1/
s/^.*: undefined .*`\([^:]*>\) .*$/\1/
w templates
d

