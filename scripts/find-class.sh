#!/bin/sh
#
# First argument is class name
# Remaining arguments are directories in the include path

dbfile=`dirname $0`/+class-database
class=$1; shift
path="$@"

# Note the removal of MICO ministl paths,
# which clash badly with system STL

build_database() {
  rm -f $dbfile
  for x in $path; do
    echo "Cataloguing $x..." >&2
    find $x -name \*.h -exec \
     egrep "(class|struct)[ 	]+[A-Za-z][A-Za-z_0-9]*([ 	]|$)" \{\} /dev/null \; | fgrep -v ministl >> $dbfile
  done
  echo >&2
}

find_in_database() {
  egrep ":.*(class|struct)[ 	]+$class([ 	]|$)" $dbfile | \
   sort | sed -e 's/:.*$//' -e '1q'
}

if [ ! -f $dbfile ]; then
  echo "Building class database..." >&2
  build_database
fi

foundfile=`find_in_database`
if [ t"$foundfile" = "t" ]; then
  if expr "$class" : "[A-Z]" > /dev/null; then
    echo "Class $class not in database -- rebuilding database..." >&2
    build_database
    foundfile=`find_in_database`
    if [ t"$foundfile" = "t" ]; then
      echo "Class $class not found" >&2
      exit 1
    fi
  else
    echo "No header for class, builtin or system type $class found" >&2
    exit 1
  fi
fi

echo $foundfile
exit 0


