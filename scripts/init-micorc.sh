#!/bin/sh
rm -f ~/.micorc

NAMING_ADDR=inet:localhost:8889
IRD_ADDR=inet:localhost:8890
MICOD_ADDR=inet:localhost:8891

echo Killing old processes...
killall nsd ird micod
sleep 1

echo Starting ird...
ird -ORBIIOPAddr $IRD_ADDR &
sleep 1

echo Starting micod...
micod -ORBIIOPAddr $MICOD_ADDR -ORBIfaceRepoAddr $IRD_ADDR &
sleep 1

echo Starting nsd...
nsd -ORBIIOPAddr $NAMING_ADDR -ORBImplRepoAddr $MICOD_ADDR -ORBIfaceRepoAddr $IRD_ADDR &
sleep 1

dir=`dirname $0`
dir=`echo $dir | sed 's,^[^/],'\`pwd\`'/&,' | sed 's,/[^/]*$,,'`

echo -ORBImplRepoAddr $MICOD_ADDR > ~/.micorc
echo -ORBNamingAddr $NAMING_ADDR >> ~/.micorc
echo -ORBIfaceRepoAddr $IRD_ADDR >> ~/.micorc

#echo Running imr...
#imr create MidiImportService unshared $dir/bin/midi-io IDL:MidiImportService:1.0

