#!/bin/sh

# def2make.sh: read a def file from $1 with some variables for link
#   details, then append a scrap of makefile to $2 that describes
#   how to link the binary

deffile=$1
outfile=$2

# deffile should contain definitions of four variables,
# as in this example:
# 
#  executable=bin/example
#  libraries="example_impl example_idl base"
#  syslibs="-ldl -lm"
#  main=example/impl/main.C
#
# Note that deffile is sourced into this script, so it can
# use any shell syntax or commands -- be careful!
#
# Also note that the library containing the executable's
# main will not be used to link the executable unless it's
# explicitly listed in the libraries variable.

if [ ! -f $deffile ]; then
  exit 0
fi

. $deffile

if [ t"$main" = t ]; then
  echo "No main object specified" >&2
  exit 1
fi

if [ t"$executable" = t ]; then
  echo "No executable specified" >& 2
  exit 1
fi

srcdir=`dirname $main`
objdir=$srcdir/+objects

srcroot=`basename $main | sed 's/\.[Cc]$//'`
object=$objdir/$srcroot.o

if [ t"$libraries" != "t" ]; then
  libs=`echo $libraries | \
   sed -e 's|^ *|lib/lib|' -e 's| *$|.a|' -e 's|  *|.a lib/lib|g'`
  libflags=`echo $libraries | sed -e 's|^ *|-l|' -e 's|  *|&-l|g'`
fi

cat >> $outfile <<EOF

EXECUTABLES	+= $executable
$outfile:	$deffile
$executable:	$libs $object
	\$(CXX) -o $executable $objdir/*.o -Llib $libflags \$(LDFLAGS) $syslibs

EOF

#cat >> $outfile <<EOF
#
#EXECUTABLES	+= $executable
#$outfile:	$deffile
#$executable:	$libs $object $objdir/+templates.o
#	scripts/template-link.sh \\
#	  $srcdir/+templates.C $objdir/+templates.o \\
#	 "\$(CXX) -c \$(CFLAGS)" \$(CXX) -o $executable $objdir/\*.o -Llib $libflags \$(LDFLAGS) $syslibs
#
#EOF

