#!/bin/sh
# run from rosegarden root
echo ; echo Code ; echo
find . \( \( -name \*.[Cch] -o -name \*.idl \) -o -name \*.cxx \) -print | xargs wc
echo ; echo Documentation ; echo
find . -name \*.txt -print | xargs wc
