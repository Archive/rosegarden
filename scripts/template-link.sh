#!/bin/sh
#
# Chris's Appalling Template Instantiator, version 2
# 
# This is a real hack.  It runs the linker, reads the output from the
# linker with the list of undefined symbols, and tries to work out
# which are from templates that should be instantiated and instantiate
# them into a C++ file.  It then links again, including that file.
# 
# First argument: file to place template instantiations in
# Second argument: object file to build instantiation file into
# Third argument: compiler and compile flags
# Remaining arguments: link line, e.g. "gcc -o output *.o library.a -llibrary2"
# (link line must already include a match for second argument's object file)

mydir=`dirname $0`
template_source=$1; shift
template_object=$1; shift
compile=$1; shift

link="$@"
linker=`basename $1`
link_parser=link-parse-$linker.sed
link_parser_path=$mydir/$link_parser
find_class=$mydir/find-class.sh

if [ ! -f $link_parser_path ]; then
  echo Linker parse file '"'$link_parser'"' not found, ignoring templates
  exec $link
fi

tmpdir=/tmp/$$
mkdir $tmpdir || exit 2

# First pass link; use the auxiliary sed script to divide errors into
# Good and Bad.  Exit if there are any Bad ones.

if $link 2> $tmpdir/link; then rm -r $tmpdir; exit 0; fi
cp $link_parser_path $tmpdir/$link_parser
( cd $tmpdir ; sed -f $link_parser link | tee non-templates )
if [ -s $tmpdir/non-templates ]; then rm -r $tmpdir; exit 1; fi

# Get a list of classes we'll need, and try to find a header for each.
  
sort $tmpdir/templates | uniq > $tmpdir/templates"_"
mv $tmpdir/templates"_" $tmpdir/templates

if [ ! -s $tmpdir/templates ]; then cat $tmpdir/link; rm -r $tmpdir; exit 0; fi

echo; echo Templates to instantiate:
cat $tmpdir/templates

classes=`cat $tmpdir/templates | sed 's/[^A-Za-z0-9_]/ /g' | fmt -1 | sort | uniq`
echo; echo Classes to locate:
echo $classes

include_path=`echo $compile | fmt -1 | grep '^-I' | sed 's/^-I//'`
echo; echo "Include path is:"; echo $include_path; echo

for class in $classes; do
  location=`$find_class $class $include_path | sed -e 's|^\./||'`
  if [ t"$location" = "t" ]; then echo No definition for $class found
  else
    echo '#include <'$location'>' >> $template_source
    cxx_file=`echo $location | sed -e 's|^\./||' -e 's/\.h$/.cxx/'`
    if [ ! -f $cxx_file ]; then
      echo "Found $class in $location"
    else
      echo "Found $class in $location and .cxx"
      echo '#include <'$cxx_file'>' >> $template_source
    fi
  fi
done

sed -e 's/^\(.*\)$/template class \1;/' $tmpdir/templates \
 -e 's/>>/> >/g' >> $template_source

echo; echo Compiling template instantiation file...

if $compile $template_source -o $template_object; then

  echo; echo Relinking...
  rm -r $tmpdir
  exec $link

else
  rm -r $tmpdir
  exit 1
fi

