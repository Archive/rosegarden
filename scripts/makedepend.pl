#!/usr/bin/perl -w
# 
# makedepend.pl <source> <output>
# 
# source = source file to read (e.g. editor/main.h)
# output = depend output file (e.g. editor/+objects/main.d)
#
# depend output file must be in same directory as proposed object
# -- objects will only be included as a target if the source ends
# in .C or .c

# Rationale:
# 
# It doesn't seem to be possible to portably persuade either
# makedepend or gcc -M to output dependencies upon headers that
# don't exist -- presumably because they couldn't know at which
# point in the include path the headers were supposed to exist.
# But we're prepared to assume a nonexistent header <a/b.h> is
# supposed to live in ./a/b.h, and to create a dependency for it
# in case it's a generated header.
# 
# (gcc has an option to assume nonexistent headers are to be
# generated, but only if they live in the same directory as the
# source.  Ours don't; they live beneath the current directory,
# at the relative path given in the #include <...> directive.)
# 
# In fact this script doesn't deal with the include path at all:
# it only writes out dependencies for headers whose directory
# component is present beneath "." .

use strict;

my ($source, $output, %headerdb, %filedb, $object);

$source = shift;
$output = shift;

sub trawl_file {

    my $input = shift;
    my $inputdir;
    local *FILE;

    return if defined $filedb{$input}; # scanned this header before
    $filedb{$input} = 1;

    open FILE, $input or do { warn "Couldn't open $input"; return; };
    $inputdir = dirname($input);

    while (<FILE>) {

	my ($directory, $depend);

	if (m, ^\s* \#include \s* <( [^<>]* ) / ( [^<>]* )> ,x) {

	    $depend = "$1/$2";
	    $directory = $1;

	} elsif (m, ^\s* \#include \s* \"( [^\"]* )\" ,x) {

	    $depend = "$inputdir/$1";
	    $directory = dirname($depend); # not the same thing as $inputdir
	}

	if (defined $directory and -d $directory) {
	    $headerdb{$depend} = 1;
	    trawl_file($depend) if -f $depend;
	}
    }
    
    close FILE;
}

trawl_file($source);

if ($source =~ m/\.[Cc]$/) {
    my $sourcedir = dirname($source);
    ($object = $source) =~
	s, ^ (?: .*/)? (.*?) \.[Cc]$ ,$sourcedir/+objects/$1.o,x;
} else {
    $object = "";
}

open OUTPUT, ">$output" or die "Can't open $output for writing";
print OUTPUT "$output $object:\t", map { "$_ " } keys %headerdb, "\n";
close OUTPUT;

sub dirname {

    my $file = shift;

    if ($file =~ m,/,) {
	$file =~ s,(^.*)/.*,$1,;
    } else {
	$file = ".";
    }

    $file;
}

