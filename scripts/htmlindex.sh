#!/bin/sh
# 
# This is a really lousy program.  Better to rewrite it in Perl,
# even if it will be longer (will it? ...there's a challenge)

echo '<HTML><HEAD><TITLE>Document index</TITLE></HEAD><BODY>'
echo '<H1>Document index</H1>'
echo '<OL>'

for x in $* ; do
  lines=`grep '<H[1-3]>' $x | head -1`;
  if [ "t$lines" != t ]; then
    hstyle=`echo $lines | sed 's/^.*\(<H[1-3]>\).*$/\1/'`
    lines=`grep "$hstyle" $x | sed -e '$q' -e 's/$/; /'`
    txtfile=`dirname $x`/`basename $x .html`.txt
    htmlbase=`echo $x | sed -e 's,^.*/doc/,,' -e 's,^doc/,,'`
    echo '<LI><A HREF="'$htmlbase'"><B>'
    echo $htmlbase | sed 's/\.html//'
    echo '</B><BR>'
    echo $lines | sed -e 's/<[^>]*>//g' -e 's/[0-9][0-9]* &nbsp;//g'
    echo '</A><BR><I>'
    echo '</I></LI><P>'
  fi
done

echo '</OL>'
echo '</BODY></HTML>'

