#!/bin/sh

# should be run from directory above (e.g. ./scripts/make-patch.sh)

mydir=`pwd`
mybase=`basename $mydir`
arcdir=$mydir"-archive"

test -d $arcdir || mkdir $arcdir
lastarc=`ls -ltr $arcdir/*tar* 2>/dev/null | tail -1 | sed 's/^.* //'`
lastdate=`echo $lastarc | sed 's/^.*\([0-9][0-9][0-9][0-9][0-9][0-9]\).*$/\1/'`

echo arcdir is $arcdir
echo last archive is $lastarc
echo last date is $lastdate

myarc=`date '+%y%m%d'`.tar
mypatch=$lastdate-`date '+%y%m%d'`.diff
echo myarc is $myarc.bz2
echo mypatch is $mypatch.bz2

(
cd ..
echo Making tarball...
tar cf $arcdir/$myarc $mybase
echo Zipping...
bzip2 $arcdir/$myarc

if [ t"$lastarc" = "t" ]; then
  echo "No last archive, can't make diff"
  exit 0
fi

echo Unpacking last archive...
cd $arcdir
bunzip2 $lastarc
tar xf `basename $lastarc .bz2`

echo Making patch...
diff -C 2 -P -r $arcdir/$mybase $mydir > $mypatch
rm -r $arcdir/$mybase

bzip2 `basename $lastarc .bz2` $mypatch
)
