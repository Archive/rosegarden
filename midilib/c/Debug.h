// -*- c-file-style:  "bsd" -*-
#ifndef _DEBUG_HEADER_
#define _DEBUG_HEADER_

#define Begin(X)
#define End
#define Return(X) return X

#define BEGIN(x)
#define END                     return
#define RETURN_INT(X) 		return X
#define RETURN_LONG(X)		return X
#define RETURN_CHAR(X) 		return X
#define RETURN_PTR(X)  		return X
#define RETURN_BOOL(X) 		return X
#define RETURN_WIDGET(X)	return X
#define ASSERT(X)

#endif /* _DEBUG_HEADER_ */

