/*

ErrorHandler.c

Generic Error-handling function for midi sequencer application.

Andy Green, 1993

*/

#include <stdio.h>
#include "MidiErrorHandler.h"
#include "MidiFile.h"
#include "Debug.h"

void ErrorHandler(error_level  Seriousness,
		  char 	      *ErrorMsg,
		  int	       LineNumber,
		  char	      *FileName)
{
BEGIN("ErrorHandler");

	switch(Seriousness)
	{
	case NON_FATAL_REPORT_TO_MSGBOX:

	case NON_FATAL_REPORT_TO_STDERR:
		
		fprintf(stderr, "Non-fatal Error: %s\n",
			ErrorMsg);

#ifdef REPORT_LINE_AND_FILE

		fprintf(stderr, "Occurred in %s at line %d.\n\n",
			FileName,
			LineNumber);
#endif

		END;

	case FATAL:

		fprintf(stderr, "Fatal Error: %s\n",
			ErrorMsg);

#ifdef REPORT_LINE_AND_FILE

		fprintf(stderr, "Occurred in %s at line %d.\n\n",
			FileName,
			LineNumber);
#endif
		exit(-1);

	default:

		Error(FATAL, "Invalid Error code");
	}
END;
}



