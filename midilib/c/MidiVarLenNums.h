// -*- c-file-style:  "bsd" -*-
/*

VarLenNums.h

Prototypes for Variable-Length number manipulation functions.

Andy Green, 1994

*/

typedef long VarLengthNum;

typedef union
{
	VarLengthNum	Number;
	byte		ByteField[4];
}
VarLengthNumBuffer;

VarLengthNum Midi_ConvFixedToVariable( long FixedFormatValue );
long Midi_ConvVariableToFixed( VarLengthNum VarLengthValue );


