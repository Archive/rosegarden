// -*- c-file-style:  "bsd" -*-
/*

ErrorHandler.h

Defines generic error handling for the midi sequencer application.

Andy Green, 1993

*/

typedef enum
{
	NON_FATAL_REPORT_TO_STDERR,
	NON_FATAL_REPORT_TO_MSGBOX,
	FATAL
}
error_level;

#define Error(X, Y) ErrorHandler(X, Y, __LINE__, __FILE__)

void ErrorHandler(error_level, char *, int, char *);

