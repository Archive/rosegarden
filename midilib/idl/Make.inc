
clean:	midilib-idl-clean
midilib-idl-clean:
	rm -f midilib/idl/*.[Ch]

MIDILIB_GENHEADERS	= $(patsubst %.idl,%.h,$(shell ls midilib/idl/*.idl))
GENHEADERS	+= $(MIDILIB_GENHEADERS)

midilib/idl/+objects/Make.gen: $(MIDILIB_GENHEADERS)
