// -*- c-file-style:  "bsd""bsd" -*-

#ifndef _MIDI_FILE_IMPL_H_
#define _MIDI_FILE_IMPL_H_

#include <midilib/idl/Midi.h>
#include <midilib/c/MidiFile.h>
#include <midilib/impl/MidiTrackImpl.h>

using namespace Rosegarden::Midi;

class MidiFileImpl : public virtual MidiFile_skel
{
public:
    MidiFileImpl();
    virtual ~MidiFileImpl();

    virtual Status *load(const char *filename);
    virtual Status *save(const char *filename);

    virtual CORBA::Short getTimeDivision();
    virtual void setTimeDivision(CORBA::Short);
    virtual MidiSMPTEGranularity getSMPTEGranularity();
    virtual CORBA::Boolean usesSMPTE();

    virtual CORBA::Short getTrackCount();
    virtual MidiTrack_ptr getTrack(CORBA::Short trackNo)
	throw (IndexOutOfRange);
    virtual void addTrack(CORBA::Short index)
	throw (IndexOutOfRange);
    virtual void deleteTrack(CORBA::Short index)
	throw (IndexOutOfRange);

    virtual MidiChannelList *getChannelList();
    virtual MidiProgramList *getProgramsPerChannel(MidiChannel channel)
    	throw (ChannelOutOfRange);
    virtual MidiTrackList *getTracksPerChannel(MidiChannel channel)
    	throw (ChannelOutOfRange);

private:
    CORBA::Boolean m_fileLoaded;
    MIDIHeaderChunk m_header;
    vector<MidiTrackImpl *> m_tracks;
};


#endif

