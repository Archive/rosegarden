// -*- c-file-style:  "bsd" -*-
#ifndef _MIDI_TRACK_IMPL_H_
#define _MIDI_TRACK_IMPL_H_

#include <midilib/idl/Midi.h>
#include <midilib/c/MidiTrack.h>

using namespace Rosegarden::Midi;

class MidiFileImpl;

class MidiTrackImpl : public virtual MidiTrack_skel
{
public:
    MidiTrackImpl(MidiFileImpl *file, EventList el);
    virtual ~MidiTrackImpl();

    MidiTrackImpl(const MidiTrackImpl &);
    MidiTrackImpl &operator=(const MidiTrackImpl &);

    virtual MidiTrack_ptr clone();
    virtual void releaseImpl();

    virtual CORBA::Long getEventCount();
    virtual MidiEvent *getEvent(CORBA::Long eventNo)
	throw (IndexOutOfRange);

    virtual MidiTrackIterator_ptr getIterator(CORBA::Long eventNo)
	throw (IndexOutOfRange);

    virtual void transpose(MidiPitch delta);
    virtual void quantize(CORBA::Boolean doPos, MidiQuantizeLevel posLevel,
			  CORBA::Boolean doDur, MidiQuantizeLevel durLevel);
    virtual void changeChannel(MidiChannel from, MidiChannel to);

    virtual void filterByChannel(MidiChannel channel);
    virtual void filterByChannels(MidiChannel channel, CORBA::Boolean retainAbove);

    // for the File -- yes, we love encapsulation
    EventList getEventList() { return m_eventList; }

private:
    MidiFileImpl *m_file;
    EventList m_eventList;

    friend class MidiTrackIteratorImpl;
};

class MidiTrackIteratorImpl : public virtual MidiTrackIterator_skel
{
public:
    MidiTrackIteratorImpl(MidiTrackImpl *track, EventList el);
    virtual ~MidiTrackIteratorImpl();

    virtual void releaseImpl() { CORBA::release(this); }

    virtual MidiTrack_ptr getTrack();
    virtual MidiTrackIterator_ptr duplicate();

    virtual CORBA::Boolean atLeftEnd();
    virtual CORBA::Boolean atRightEnd();

    virtual CORBA::Boolean goLeft();
    virtual CORBA::Boolean goRight();

    virtual CORBA::Boolean getRightElement(MidiEvent *&);
    virtual CORBA::Boolean nextRight(MidiEvent *&);

    virtual void insert(const MidiEvent &);
    virtual CORBA::Boolean deleteLeft();
    virtual CORBA::Boolean deleteRight();

private:
    MidiTrackImpl *m_track;
    EventList operationPtr();

    // This pointer is not what you might think.
    // See comments in the .C file.
    EventList m_eventPtr;
};

class MidiEventConverter
{
public:
    static bool internalToPublic(MIDIEventStruct *in, MidiEvent &out);
    static bool publicToInternal(MidiEvent in, MIDIEventStruct *&out);

    static int quantizeLevelToInt(MidiQuantizeLevel level);
    static MidiQuantizeLevel intToQuantizeLevel(int level);
};

#endif
