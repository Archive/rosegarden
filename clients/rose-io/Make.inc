
# these are safe to delete
PCCTS_SRC	+= \
	clients/rose-io/AParser.C \
	clients/rose-io/ATokenBuffer.C \
	clients/rose-io/DLexerBase.C

LIBRARIES	+= lib/libclients_rose-io.a

# rather brute-force -- main.C requires RoseParser.h, so
# include a rule for it and that should guarantee the PCCTS-
# generated files get build rules in the Make.gen.compile

clients/rose-io/RoseParser.h:	clients/rose-io/RosePCCTS.g $(PCCTS_SRC)
	cd clients/rose-io ; \
	$(ANTLR) $(ANTLRFLAGS) -fl RoseLexer.dlg -ft RoseParserTokens.h \
	  RosePCCTS.g ; \
	$(DLG) $(DLGFLAGS) -cl RoseLexer RoseLexer.dlg ; \
	for x in RoseLexer.cpp RoseParser.cpp RosePCCTS.cpp ; \
	  do test -f $$x && mv $$x `basename $$x .cpp`.C ; \
	done

clients/rose-io/%.C:	$(PCCTSINCDIR)/%.cpp
	cp $< $@

clients/rose-io/%.h:	$(PCCTSINCDIR)/%.h
	cp $< $@

clients/rose-io/%.cpp:	$(PCCTSINCDIR)/%.cpp
	cp $< $@

#clean:	rose-io-clean
distclean:	rose-io-clean

rose-io-clean:
	rm -f $(PCCTS_SRC) \
	  clients/rose-io/*.cpp \
	  clients/rose-io/*.dlg \
	  clients/rose-io/RoseLexer.C \
	  clients/rose-io/RoseLexer.h \
	  clients/rose-io/RoseParser.C \
	  clients/rose-io/RoseParser.h \
	  clients/rose-io/RoseParserTokens.h \
	  clients/rose-io/RosePCCTS.C
	for x in clients/rose-io/*.h ; do \
	  if [ -f $(PCCTSINCDIR)/`basename $$x` ]; then rm -f $$x ; fi ; \
	done

