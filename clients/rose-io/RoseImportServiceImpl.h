// -*- c-file-style:  "bsd" -*-
#ifndef _ROSE_IMPORT_SERVICE_IMPL_H_
#define _ROSE_IMPORT_SERVICE_IMPL_H_

#include "server/idl/RosegardenServer.h"

//!!! NOT REENTRANT -- currently uses four global variables; must be fixed

class RoseImportServiceImpl :
    virtual public Rosegarden::Server::ImportService_skel
{
public:
    RoseImportServiceImpl() { }
    virtual ~RoseImportServiceImpl() { }

    virtual CORBA::WChar *getFormatName() {
	return CORBA::wstring_dup(L"Rosegarden 2.1 file");
    }

    virtual char *getFormatExtensions() {
	return CORBA::string_dup("rose");
    }

    virtual Rosegarden::Status *doImport
    (const char *filename, Rosegarden::Server::Composition_ptr composition);
};

#endif

