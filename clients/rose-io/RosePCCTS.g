
#header <<

// some of the things I return are objects -- can't just initialise
// them by overwriting with zeros!
#define PURIFY(r,s)

#include <AToken.h>

#include <iostream.h>

#include <common/my_wstring.h>
#include <server/idl/RosegardenServer.h>
#include <server/wrapper/RosegardenServerWrapper.h>
#include <utilities/idl/RosegardenUtilities.h>

typedef pair<int, int> Int2Pair;
typedef vector<Int2Pair> Int2PairList;

//typedef Assoc<Int2Assoc, TimeSignature> Int2TimeSigAssoc;
typedef ANTLRCommonToken ANTLRToken;

using namespace Rosegarden;
using namespace Rosegarden::Server;

// this is what we fill
extern Composition_ptr parserComposition;

extern ElementFactory_ptr parserElementFactory;
extern GroupInfoFactory_ptr parserGroupInfoFactory;
extern Utilities_ptr parserUtilities;

>>

#lexclass START
#token "\r" << skip(); >>
#token "\n" << skip(); newline(); >>
#token HEADER_TAG "\#\!Rosegarden\n\#\n\#\ \ Musical\ Notation\ File\n\#\n" 
    << newline(); newline(); newline(); newline(); >>
#token VERSION_TAG "RV21" << mode(NORMAL); >>

#lexclass NORMAL
#token "[\ \t\r]" << skip(); >>
#token "\n" << skip(); newline(); >>

#token DOTTED_TAG "Dotted"
#token MARK_START_TAG "Mark\ start"
#token MARK_END_TAG "Mark\ end"
#token GROUP_TAG "Group"
#token BEAMED_TAG "Beamed"
#token TUPLED_TAG "Tupled"
#token END_TAG "End"
#token CLEF_TAG "Clef"
#token KEY_TAG "Key"
#token TEXT_TAG "Text"
#token POSITION_TAG "Position"
#token REST_TAG "Rest"
#token REST_LC_TAG "rest"
#token METRONOME_TAG "Metronome"
#token PRECEDES_TAG "Precedes"
#token FOLLOWS_TAG "Follows"
#token STAFF_TAG "Staff"
#token STAVE_TAG "Stave"
#token BAR_TAG "Bar"
#token TIME_TAG "time"
#token TAGS_TAG "tags"
#token CONNECTED_TAG "connected"
#token PROGRAM_TAG "program"
#tokclass STAVES_TAG { "Staffs" "Staves" }
#tokclass CHORD_TAG { "Chord" ":" }

// String delimiters
#token NAME_TAG "Name\ " << skip(); mode(UGLY_STRING_CLASS); >>
#token STRING_TAG "String\ " << skip(); mode(UGLY_STRING_CLASS); >>
#token STRING_QUOTE "\"" << skip(); mode(QUOTED_STRING_CLASS); >>

#token WORD "[A-Za-z] ~[\ \t\n]*"
#token POSITIVE_INTEGER "[0-9]+"
#token NEGATIVE_INTEGER "\-[0-9]+"

// Might as well introduce a comment syntax
#token COMMENT_TAG "#" << skip(); mode(COMMENT_CLASS); >>

#lexclass UGLY_STRING_CLASS
#token UGLY_STRING "[\n@]" << replchar('\0'); newline(); mode(NORMAL); >>
#token "~[]" << more(); >>

#lexclass QUOTED_STRING_CLASS
#token "\\\"" << more(); replchar('\"'); >>
#token GOOD_STRING "\"" << replchar('\0'); mode(NORMAL); >>
#token BAD_STRING "\n" << replchar('\0'); newline(); mode(NORMAL); /*!!!*/ >>
#token "~[]" << more(); >>

#lexclass COMMENT_CLASS
#token COMMENT "[\n@]" << skip(); mode(NORMAL); >>
#token "~[]" << skip(); >>

#tokclass STRING { WORD GOOD_STRING BAD_STRING UGLY_STRING }
#tokclass INTEGER { POSITIVE_INTEGER NEGATIVE_INTEGER }

#lexclass NORMAL


class RoseParser {

<<
    TransactionId m_transactionId;
>>

composition
    :
	<<
	   if (!CORBA::is_nil(parserElementFactory)) {
	     CORBA::release(parserElementFactory);
	   }
	   if (!CORBA::is_nil(parserGroupInfoFactory)) {
	     CORBA::release(parserGroupInfoFactory);
	   }
	   parserElementFactory = parserComposition->getElementFactory();
	   parserGroupInfoFactory = parserComposition->getGroupInfoFactory();
	>>
	HEADER_TAG VERSION_TAG
	composition_body
	time_signatures
	staff_meta_data
	END_TAG
	"@"
	<<
	     CORBA::release(parserElementFactory);
	     parserElementFactory = ElementFactory::_nil();
	     CORBA::release(parserGroupInfoFactory);
	     parserGroupInfoFactory = GroupInfoFactory::_nil();
	>>	   
    ;

composition_body
    :	<< int staffs; >>
	staff_count > [staffs]
	( staff ) *
    ;

time_signatures
    :
	( BAR_TAG positive_integer positive_integer
	  TIME_TAG positive_integer positive_integer
	) *
    ;	

/*!!!
time_signatures > [List<Int2TimeSigAssoc> tsl]
    :   << int staffNo; int barNo; int num; int denom; >>
	( BAR_TAG positive_integer > [staffNo] positive_integer > [barNo]
	  TIME_TAG positive_integer > [num] positive_integer > [denom]
	  << Assoc<int, int> sb(staffNo, barNo);
	     $tsl.append(Int2TimeSigAssoc(sb, TimeSignature(num, denom)));
	  >>
	) *
    ;
*/
staff_meta_data
    :
	( STAVE_TAG positive_integer
	  { TAGS_TAG positive_integer positive_integer }
	  { CONNECTED_TAG positive_integer }
	  { PROGRAM_TAG positive_integer }
	) *
    ;

staff_count > [int i]
    :
	<< $i = 0; >>
	STAVES_TAG integer > [$i]
    ;

staff > [CORBA::Short pc]
    :
	<< my_wstring staffName; ElementList el;
	   // need to dup composition here, as the wrapper seizes its copy
	   CompositionWrapper cw(Composition::_duplicate(parserComposition));
	   Transaction tr(cw, "Load Rosegarden 2.1 file",
		PartBasicIterator::_nil(), PartBasicIterator::_nil());
	   m_transactionId = tr;
	>>

	staff_name > [staffName]
	item_list > [el]

	<< $pc = cw->getPartCount();
	   cw->createPart(tr, $pc);
	   Part_var p = cw->getPart($pc);
	   PartBasicMutatorWrapper pm = p->getBasicMutator();
	   int i;
	   for (i = 0; i < el.length(); ++i) {
	      assert(!CORBA::is_nil(el[i]));
	      pm->insert(tr, el[i]);  // to release or not to release?
				// ... the answer, by experiment, is not to release
	   }
	   tr.end();
	>>
    ;

staff_name > [my_wstring n]
    :
	{ STAFF_TAG } string > [$n]
    ;

item_list > [ElementList el]
    :
	<< Element_ptr e = Element::_nil();
	   ElementList gl;
	   int i = 0, j;
	>>
	( item > [e]
	  << if (!CORBA::is_nil(e)) {
	       $el.length(i + 1);
	       $el[i] = e;
	       ++i;
	     }
	  >>
	| group > [gl]
	  << for (j = 0; j < gl.length(); ++j) {
	       assert(!CORBA::is_nil(gl[j]));
	       $el.length(i + 1);
	       $el[i] = gl[j];
	       ++i;
	     }
	  >>
	) *
	END_TAG
    ;

group > [ElementList el]
    :
	<< int i; >>

	GROUP_TAG
	( ( BEAMED_TAG
	    item_list > [$el]
	    <<
	       for (i = 0; i < $el.length(); ++i) {
	         assert(!CORBA::is_nil($el[i]));
	         parserGroupInfoFactory->setBeamedGroupInfo
	           (m_transactionId, $el[i], i == 0, i == $el.length()-1);
	       }
	    >>
	  )
	| ( TUPLED_TAG
	    << int tupled_length; int tupled_count; >>
	    positive_integer > [tupled_length]
	    positive_integer > [tupled_count]
	    item_list > [$el]
	    <<
	       for (i = 0; i < $el.length(); ++i) {
	         assert(!CORBA::is_nil($el[i]));
	         parserGroupInfoFactory->setTupledGroupInfo
	           (m_transactionId, $el[i], i == 0, i == $el.length()-1,
	             0, tupled_length * 6, tupled_count); //!!! duration??
	       }
	    >>
	  )
	)
    ;

item > [Element_ptr i]
    :
	<< $i = Element::_nil(); >>
	simple_item > [$i]
	  ( mark_start | mark_end ) *
	  ( precedes_follows_spec ) *
    ;

simple_item > [Element_ptr i]
    :
	<< $i = Element::_nil(); >>
	clef_item > [$i]
    |	key_item > [$i]
    |	text_item > [$i]
    |	chord_item > [$i]
    |	rest_item > [$i]
    |	metronome_item > [$i]
    ;

precedes_follows_spec
    :
	( PRECEDES_TAG string
	<< cerr << "Warning: Precede tags not yet implemented" << endl; >>
	)
    |	( FOLLOWS_TAG string
	<< cerr << "Warning: Follow tags not yet implemented" << endl; >>
	)
    ;

mark_start
    :
	<< int i; my_wstring type; >>
	MARK_START_TAG
	positive_integer > [i]
	string > [type]
	<< cerr << "starting a mark, index " << i << " and type " << type << endl; >>
    ;

mark_end
    :
	<< int i; >>
	MARK_END_TAG
	positive_integer > [i]
	<< cerr << "ending a mark, index " << i <<  endl; >>
    ;

clef_item > [ClefChange_ptr c]
    :
	<< my_wstring s; $c = ClefChange::_nil(); >>

	CLEF_TAG { CLEF_TAG } string > [s]

	<< cout << "Clef is \"" << s << "\"" << endl; >>

	<< {
	   CORBA::WChar *w = CORBA::wstring_dup(s.c_str());
	   $c = parserElementFactory->newClefChange
	       (parserUtilities->rose21ClefNameToClef(w), 0);
	   CORBA::wstring_free(w);
	} >>
    ;


key_item > [KeyChange_ptr k]
    :
	<< my_wstring s; $k = KeyChange::_nil(); >>

	KEY_TAG { KEY_TAG } string > [s]

	<< cout << "Key is \"" << s << "\"" << endl; >>

	<< {
	   CORBA::WChar *w = CORBA::wstring_dup(s.c_str());
	   $k = parserElementFactory->newKeyChange
	       (parserUtilities->rose21KeyNameToKey(w));
	   CORBA::wstring_free(w);
	} >>
    ;

text_item > [Text_ptr t]
    :
	<< int position; my_wstring text; $t = Text::_nil(); >>

	TEXT_TAG string > [text] POSITION_TAG positive_integer > [position]

	<< TextType type;
	   switch (position) {
	     case 0: type = MinorDirectionText; break; // "AboveStave"
	     case 1: type = PrincipalDirectionText; break; // "AboveStaveLarge"
	     case 2: type = BarLabelText; break; // "AboveBarLine"
	     case 3: type = LyricText; break; // "BelowStave"
	     case 4: type = DynamicDirectionText; break; // "BelowStaveItalic"
	     case 5: type = ChordLabelText; break; // "ChordName"
	     case 6: type = DynamicText; break; // "Dynamic"
	   }

	   {
	   CORBA::WChar *w = CORBA::wstring_dup(text.c_str());
	   $t = parserElementFactory->newText(type, w);
	   CORBA::wstring_free(w);
	   }
	>>
    ;

chord_item > [Chord_ptr c]
    :
	<< Time dur; int mods; Int2PairList iil; $c = Chord::_nil(); >>
	CHORD_TAG
	chord_duration > [dur]
	positive_integer > [mods]
	chord_voice_list > [iil]
	<< {
	NoteList vl;
	vl.length(iil.size());
	for (int i = 0; i < iil.size(); ++i) {

	  Accidental a = NoAccidental;
	  if (iil[i].first & 1) a = Sharp;
	  if (iil[i].first & 2) a = Flat;
	  if (iil[i].first & 4) a = Natural;

 	  //!!! Pitch will be wrong!  We need the correct clef!
	  vl[i] = parserElementFactory->newNote
	    (dur, parserUtilities->rose21PitchToPitch
	      (iil[i].second, TrebleClef, a), a,
		DefaultVelocity);
	}
	$c = parserElementFactory->newChord(dur, vl);
	} >>
    ;

chord_duration > [Time d]
    :
	<< my_wstring text; >>
	{ dt:DOTTED_TAG }
	string > [text]
	<< {
	   CORBA::WChar *w = CORBA::wstring_dup(text.c_str());
	   $d = parserUtilities->rose21NoteNameToTime(w, $dt != NULL);
	   CORBA::wstring_free(w);
	} >>
    ;

chord_voice_list > [Int2PairList vl]
    :
	<< int modifier, pitch, count; >>
	integer > [count]
	( integer > [pitch]
	  positive_integer > [modifier]
	  << $vl.push_back(Int2Pair(modifier, pitch)); >>
	) *
	<< if ($vl.size() != count)
	      cerr <<  "Warning: Chord has wrong number of voices (says "
	           << count << ", has " << $vl.size() << ")" << endl;
	>>
    ;

rest_item > [Rest_ptr r]
    :
	<< $r = Rest::_nil(); Time d; >>
	REST_TAG chord_duration > [d] REST_LC_TAG
	<< $r = parserElementFactory->newRest(d); >>
    ;

metronome_item > [TempoChange_ptr tc]
    :
	<< $tc = TempoChange::_nil(); Time d; int b; >>
	METRONOME_TAG chord_duration > [d] positive_integer > [b]
	<< $tc = parserElementFactory->newTempoChange(d, b); >>
    ;

integer > [int i]
    :
	str:INTEGER << $i = atoi($str->getText()); >>
    ;

positive_integer > [unsigned int i]
    :
	str:POSITIVE_INTEGER
	<< {
	    int si;
	    si = atoi($str->getText());
	    $i = (unsigned)si;
	} >>
    ;

string > [my_wstring s]
    :
	str:STRING << $s = $str->getText(); >>
    ;

}

