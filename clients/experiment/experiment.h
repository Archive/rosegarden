// -*- c-file-style:  "bsd" -*-
#ifndef EXPERIMENTS_H_
#define EXPERIMENTS_H_

#include <iostream>

struct A
{
    A(int i) : m_i(i) { }

    virtual void show() const { std::cout << "A " << m_i << std::endl; }
    virtual A *clone() const { return new A(*this); }

    int m_i;
};

struct B : A
{
    B(int i, int j) : A(i), m_j(j) { }

    virtual void show() const { std::cout << "B " << m_j << " in "; A::show(); }
    virtual A *clone() const { return new B(*this); }

    int m_j;
};


#endif

