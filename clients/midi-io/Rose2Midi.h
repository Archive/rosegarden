// -*- c-file-style:  "bsd" -*-
#ifndef _ROSE_2_MIDI_H_
#define _ROSE_2_MIDI_H_

// See comments in Midi2Rose.h

#include <midilib/idl/Midi.h>
#include <server/idl/RosegardenServer.h>
#include <utilities/idl/RosegardenUtilities.h>

class Rose2MidiConverter
{
public:
    Rose2MidiConverter(Rosegarden::Utilities_ptr rup, short timeDivision);
    virtual ~Rose2MidiConverter();

    // NoCorrespondingMidiEvent is thrown for rests, amongst other things
    class NoCorrespondingMidiEvent { };
    Rosegarden::Midi::MidiEvent convert(Rosegarden::Server::Element_ptr element)
	throw (NoCorrespondingMidiEvent);

private:
    Rosegarden::Utilities_ptr m_utilities;
    short m_timeDivision;
};		 


#endif

