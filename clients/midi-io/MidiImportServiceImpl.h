// -*- c-file-style:  "bsd" -*-
#ifndef _MIDI_IMPORT_SERVICE_IMPL_H_
#define _MIDI_IMPORT_SERVICE_IMPL_H_

#include "server/idl/RosegardenServer.h"

class MidiImportServiceImpl :
    virtual public Rosegarden::Server::ImportService_skel
{
public:
    MidiImportServiceImpl() { }
    virtual ~MidiImportServiceImpl() { }

    virtual CORBA::WChar *getFormatName() {
	return CORBA::wstring_dup(L"MIDI file");
    }

    virtual char *getFormatExtensions() {
	return CORBA::string_dup("mid(i)?");
    }

    virtual Rosegarden::Status *doImport
    (const char *filename, Rosegarden::Server::Composition_ptr composition);
};

#endif

