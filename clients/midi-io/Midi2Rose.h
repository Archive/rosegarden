// -*- c-file-style:  "bsd" -*-
#ifndef _MIDI_2_ROSE_H_
#define _MIDI_2_ROSE_H_

// I'd rather like to avoid any includes at all, but
// that would mean using references or pointers for
// all the arguments and forward-declaring the types.
// 
// Fine, except that _ptr types are typedefs for
// pointers, not classes, so you can't forward-declare
// them.  And you're not allowed to assume they *are*
// pointers and just shortcut the typedef.
// 
// We could use _var instead, but that'd be grim too.

#include <midilib/idl/Midi.h>
#include <server/idl/RosegardenServer.h>
#include <utilities/idl/RosegardenUtilities.h>

class Midi2RoseConverter
{
public:
    Midi2RoseConverter(Rosegarden::Server::ElementFactory_ptr efp,
		       Rosegarden::Utilities_ptr rup,
		       short timeDivision);
    virtual ~Midi2RoseConverter();

    Rosegarden::Server::Element_ptr convert(Rosegarden::Midi::MidiEvent &event);

private:
    Rosegarden::Server::ElementFactory_ptr m_elementFactory;
    Rosegarden::Utilities_ptr m_utilities;
    short m_timeDivision;
};		 


#endif

