// -*- c-file-style:  "bsd" -*-
#ifndef _MIDI_EXPORT_SERVICE_IMPL_H_
#define _MIDI_EXPORT_SERVICE_IMPL_H_

#include "server/idl/RosegardenServer.h"

class MidiExportServiceImpl :
    virtual public Rosegarden::Server::ExportService_skel
{
public:
    MidiExportServiceImpl() { }
    virtual ~MidiExportServiceImpl() { }

    virtual CORBA::WChar *getFormatName() {
	return CORBA::wstring_dup(L"MIDI file");
    }

    virtual char *getFormatExtensions() {
	return CORBA::string_dup("mid(i)?");
    }
    
    virtual char *getDefaultFormatExtension() {
	return CORBA::string_dup("mid");
    }

    virtual Rosegarden::Status *doExport
    (const char *filename, Rosegarden::Server::Composition_ptr composition);
};

#endif

