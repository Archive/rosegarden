// -*-Mode: C++;-*-
#ifndef _ROSEGARDEN_PERFORMER_IDL_
#define _ROSEGARDEN_PERFORMER_IDL_

// Rosegarden Performer IDL

#include <utilities/idl/RosegardenCommon.idl>

module Rosegarden {
module Performer {

// Like to split these definitions to mark the distinction between
// Studio and Performance more clearly

typedef short Gain;             // 0 - 100, Microphones
typedef short MixerChannel;     // 64 say
typedef long InstrumentHandle;  // as many as you like
typedef short MicrophoneHandle; // 64 say


// Volumes for everything.  Mixer, Microphone gain, Amps, Instruments,
// Performances and Master control
typedef short Volume;     // 0 - 100
typedef short VolumeStep; // 0 - 100

//typedef short Tempo;
typedef short TempoStep;

// Define sound positioning for Instruments and Microphones.
// Leaving this defn loose for the moment.
struct SoundPosition
{
  short x;  // stereo...
  short y;  // for that four speaker bonanza...
  short z;  // well, you never know...
};

// Again, loose for the moment.
typedef SoundPosition PhysicalPosition;

// TBD how to represent this consistently in the server.
// This will probably become an interface.
enum TempoType
{
  MIDIClicks,
  BPM,
  BPS  // etc.
};

enum TempoMode
{
  Independent,
  WithParent,
  RelativetoParent
};

// TBD as above
struct Tempo
{
  long tempo;
  TempoType type;
};

// allow PerformerDynamics to affect the PerformerNote
// as well as the Instrument, the Musician and the Conductor.
struct PerformerNoteDynamic
{
  long pitchbend;
  long portamento; // etc.
};

// Instruments to be affected by the Composition within the
// context of the Performance - this suggests a need for 
// dynamic Instruments or some kind of Instrument memory.
// Maybe.
struct PerformerInstrumentDynamic
{
  long detune;
  long muffle; // etc.
};

struct PerformerNote
{
  long pitch;
  long duration;
  long instrument;
  short performance;
  PerformerNoteDynamic dynamic;
};


struct Microphone
{
  PhysicalPosition microphonePhysicalPosition;
  SoundPosition microphoneSoundPosition;
  Gain microphoneGain;
};


exception OutOfMemoryException { wstring reason; };
exception OverflowException { wstring reason; };
exception TruncationException { wstring reason; };
exception OutOfRangeException { wstring reason; };

// everything that processes sound is a VolumeController
interface VolumeController
{
    // stereo position
    void setSoundPosition(in SoundPosition sp) raises (OutOfRangeException);
    SoundPosition getSoundPosition();
    
    // volume controls absolute
    void setVolume(in Volume v) raises (OutOfRangeException);
    Volume getVolume();
    void maxVolume();
    void mute();
    void unMute();
    boolean isMute();

    // volume controls relative in steps
    Volume incrementVolume(in short steps);
    Volume decrementVolume(in short steps);
    void setVolumeStep(in VolumeStep vs) raises (OutOfRangeException);
    VolumeStep getVolumeStep();
  };

  interface TempoController
  {
    Tempo getTempo();
    void setTempo(in Tempo t) raises (OutOfRangeException);
    Tempo incrementTempo(in short steps);
    Tempo decrementTempo(in short steps);
    void setTempoStep(in TempoStep ts) raises (OutOfRangeException);
    TempoStep getTempoStep();

    TempoMode getTempoMode();
    void setTempoMode(in TempoMode tm) raises (OutOfRangeException);
  };


  interface TimbreController
  {
  };

  interface Instrument : VolumeController
  {
    wstring getName();
    void setName(in wstring s) raises (OverflowException);
    void playNote();
  };

/*
  interface Performance : TempoController, VolumeController
  {
    Tempo t;
    //Location l;  // acoustic properties of location
    // other performance related stuff
  };
*/

  typedef sequence<Instrument> InstrumentList;
  //typedef sequence<Performance> PerformanceList;

  // The top level interface used for Playback and Recording
  interface Performer : TempoController, VolumeController
  {
    // a Performer
    void newPerformance() raises (OutOfMemoryException, OutOfRangeException);

    void playNote(in PerformerNote pn) raises (OutOfRangeException);

    //void sendConductor();     // directions from the Server to the Conductor
    //void receiveConductor();  // directions from the Conductor to the Server
    
    void getInstruments(out InstrumentList il);
    //void getPerformances(out PerformanceList pl);
  };

  interface Musician : TempoController, VolumeController
  {
    void addInstrument();
    // modifier on Intrument or Group of Instruments

    InstrumentList getInstruments();
  };

  interface MusicianGroup : TempoController, VolumeController
  {
    void addMusician(in Musician m);
    void deleteMusician();

    // modifiers on MusicianGroup
  };

  interface Conductor : TempoController, VolumeController
  {
  };

  
  interface Studio : VolumeController
  {
     void addMicrophone();
     void deleteMicrophone();
     void setMicrophone(in MicrophoneHandle mh);  // positions, gain, etc.
     void getMicrophones();

     void connect();  // connect Microphone or Instrument
   };

};
};

#endif // _ROSEGARDEN_PERFORMER_IDL_
