
clean:	performer-idl-clean
performer-idl-clean:
	rm -f performer/idl/*.[Ch]

PERFORMER_GENHEADERS	= $(patsubst %.idl,%.h,$(shell ls performer/idl/*.idl))
GENHEADERS	+= $(PERFORMER_GENHEADERS)

performer/idl/+objects/Make.gen: $(PERFORMER_GENHEADERS)
