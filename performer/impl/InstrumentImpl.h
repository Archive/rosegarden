// -*- c-file-style:  "bsd" -*-
#ifndef _INSTRUMENT_IMPL_H_
#define _INSTRUMENT_IMPL_H_

#include <performer/idl/RosegardenPerformer.h>
//#include <server/idl/RosegardenServer.h>
#include <common/my_wstring.h>
#include <performer/impl/PerformerCommon.h>

using namespace Rosegarden::Performer;

class InstrumentImpl : virtual public Instrument_skel
{
public:
  InstrumentImpl();
  virtual ~InstrumentImpl();

  // modifications in here

  virtual CORBA::WChar *getName() { return CORBA::wstring_dup(m_name.c_str()); }

  virtual void setName(const CORBA::WChar *name);

  virtual void setSoundPosition(const SoundPosition& sp);
  virtual SoundPosition getSoundPosition() { return m_position; }

  virtual void setVolume(Volume v) throw (OutOfRangeException);
  virtual Volume getVolume() { return m_volume; }
  virtual void maxVolume() { m_volume = rgp_max_volume; }
  virtual void mute() { m_mute = true; }
  virtual void unMute() { m_mute = false; }
  virtual CORBA::Boolean isMute() { return m_mute; }

  virtual Volume incrementVolume(CORBA::Short steps);
  virtual Volume decrementVolume(CORBA::Short steps);
  virtual void setVolumeStep(VolumeStep vs) throw (OutOfRangeException);
  virtual VolumeStep getVolumeStep() { return m_volumeStep; }

private:

  my_wstring m_name;
  Volume m_volume;
  VolumeStep m_volumeStep;
  bool m_mute;
  SoundPosition m_position;

};


#endif // _INSTRUMENT_IMPL_H_
