// -*- c-file-style:  "bsd" -*-#ifndef _INSTRUMENT_FACTORY_H_
#define _INSTRUMENT_FACTORY_H_

class InstrumentFactory {
public:

  InstrumentFactory();
  virtual ~InstrumentFactory();

  newInstrument(InstrumentType instrument);

private:

};

#endif // _INSTRUMENT_FACTORY_H_
