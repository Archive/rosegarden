// -*- c-file-style:  "bsd" -*-
#ifndef _PERFORMER_COMMON_H_
#define _PERFORMER_COMMON_H_

// Constants and other useful performer global stuff that we'll
// take out to an interface in time

// volume
const unsigned short rgp_max_volume=100;
const unsigned short rgp_default_volume_step=10;
const unsigned short rgp_default_volume=80;
const bool rgp_default_muted = false;

//const unsigned int rgp_max_tempo = 500;

// sound position ceiling
const short rgp_min_x_sound_position=-50;
const short rgp_max_x_sound_position=50;
const short rgp_min_y_sound_position=-50;
const short rgp_max_y_sound_position=50;
const short rgp_min_z_sound_position=-50;
const short rgp_max_z_sound_position=50;

// physical position ceiling
const short rgp_min_x_physical_position=-50;
const short rgp_max_x_physical_position=50;
const short rgp_min_y_physical_position=-50;
const short rgp_max_y_physical_position=50;
const short rgp_min_z_physical_position=-50;
const short rgp_max_z_physical_position=50;

#endif // _PERFORMER_COMMON_H_
