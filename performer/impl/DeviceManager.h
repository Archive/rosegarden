// -*- c-file-style:  "bsd" -*-
#ifndef _DEVICE_MANAGER_H_
#define _DEVICE_MANAGER_H_

#include <list>
#include "MusicService.h"
#include "SoundDriver.h"

// The device/driver problem can get circular if you let it.

enum DriverType
{
  OSS,
  ALSA
};


class DeviceManager {
public:

  DeviceManager();
  virtual ~DeviceManager();

  // poll the sound drivers and devices on the system to
  // see what's available.  This method also builds the
  // services list
  void acquireDevices();

private:
  void enableDriver(const DriverType& device);
  void disableDriver(const DriverType& device);

  // instrument list
  list<MusicService*> m_musicServices;

};


#endif // _DEVICE_MANAGER_H_
