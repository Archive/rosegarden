// -*- c-file-style:  "bsd" -*-
#ifndef _SOUND_DRIVER_H_
#define _SOUND_DRIVER_H_

// a proforma for the OSS and ALSA device drivers (see rosegarden/driver)

#include <common/my_wstring.h>

class SoundDriver
{
public:
  SoundDriver(my_wstring deviceName) {}
  virtual ~SoundDriver() {}

  virtual void getCapabilities() = 0;
  virtual void getInstruments() = 0;

private:
  my_wstring m_name;
};


#endif // _SOUND_DRIVER_H_
