// -*- c-file-style:  "bsd" -*-
#ifndef _PERFORMER_IMPL_H_
#define _PERFORMER_IMPL_H_

#include <performer/idl/RosegardenPerformer.h>
#include <common/my_wstring.h>
#include "PerformerCommon.h"
#include "DeviceManager.h"

using namespace Rosegarden::Performer;

// Performer Class

class PerformerImpl : virtual public Performer_skel
{
public:
  PerformerImpl();
  virtual ~PerformerImpl();

  void newPerformance() throw
                          (OutOfMemoryException, OutOfRangeException);

  virtual void getInstruments(InstrumentList *&il);
  //virtual void getPerformances(PerformanceList *pl);

  // route Notes coming into the Performer
  virtual void playNote(const PerformerNote& pn) throw (OutOfRangeException)
      { return; }


  // sound position and volume
  virtual void setSoundPosition(const SoundPosition& sp)
      throw (OutOfRangeException);

  virtual SoundPosition getSoundPosition() { return m_masterPosition; }

  virtual void setVolume(Volume v) throw (OutOfRangeException);
  virtual Volume getVolume() { return m_masterVolume; }
  virtual void maxVolume() { m_masterVolume = rgp_max_volume; }
  virtual void mute() { m_masterMute = true; }
  virtual void unMute() { m_masterMute = false; }
  virtual CORBA::Boolean isMute() { return m_masterMute; }

  virtual VolumeStep getVolumeStep() { return m_masterVolumeStep; }
  virtual Volume incrementVolume(CORBA::Short steps);
  virtual Volume decrementVolume(CORBA::Short steps);
  virtual void setVolumeStep(VolumeStep vs)
      throw (OutOfRangeException);


  virtual Tempo getTempo() { return m_masterTempo; }
  virtual void setTempo(const Tempo& t) throw (OutOfRangeException);
  virtual Tempo incrementTempo(CORBA::Short steps);
  virtual Tempo decrementTempo(CORBA::Short steps);
  virtual void setTempoStep(TempoStep ts)
      throw (OutOfRangeException);
  virtual TempoStep getTempoStep() { return m_masterTempoStep; }

  virtual TempoMode getTempoMode() { return Independent; }
  virtual void setTempoMode(TempoMode tm)
      throw (OutOfRangeException) { return; }  //does nothing at this level

private:

  //Studio m_studio;
  //PerformanceManager m_performanceManager;
  DeviceManager *m_deviceManager;

  bool m_masterMute;
  Volume m_masterVolume;
  VolumeStep m_masterVolumeStep;
  SoundPosition m_masterPosition;
  Tempo m_masterTempo;
  TempoStep m_masterTempoStep;

};

#endif // _PERFORMER_IMPL_H_
