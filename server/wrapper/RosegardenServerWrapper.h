// -*- c-file-style:  "bsd" -*-
#ifndef _ROSEGARDEN_SERVER_WRAPPER_H_
#define _ROSEGARDEN_SERVER_WRAPPER_H_

#include <server/idl/RosegardenServer.h> // boo!
#include <common/my_wstring.h>

//#define DEBUG 1

namespace Rosegarden {
namespace Server {

// {{{ Usage notes

/*

  Some random usage notes:
  
  * There's no Part wrapper at the time of writing, but even if there
    was, it couldn't do much to protect you from a Part being deleted
    while you have a reference to it.  Parts are not locked; mostly
    the server will not delete a Part with extant Iterators, but it
    doesn't care how many Part references exist.  Thus every time you
    call a Part method, you must do so in a try() block and catch the
    exception that would arise if the Part had been deleted (presently
    CORBA::OBJECT_NOT_EXIST, though a future wrapper may define its
    own exception to distinguish this expected case from the unusual).

  * If a client attempts to delete a Part using these wrappers, the
    server will at least attempt to wait until all Iterators extant on
    that Part have been relinquished.  It only has limited patience,
    though, so you may find an Iterator refers to a nonexistent Part.
    You should always check isOK() on any Iterator that hasn't been
    used for a while, before you start using it again.  (Each method
    call made will refresh the server's patience, so Iterators that
    are used regularly have nothing to fear.)

  --cc,19981207

*/

// }}}

// {{{ Wrapper base

// The m_implBackup stuff follows a pattern from mico/var.h:

template <class T = CORBA::Object, class Tptr = CORBA::Object_ptr>
class LockingWrapperBase
{
public:
    virtual ~LockingWrapperBase() {
	// annoying.  finalise() is virtual, and we can only call this
	// class's implementation from the destructor.  so subclasses
	// are going to have to call finalise() from their own dtors
	// too, and then reset m_impl and m_implBackup to nil so as to
	// neuter this dtor...  The STANDARD_WRAPPER_METHODS do this
	if (m_impl != m_implBackup) {
	    finalise(m_implBackup);
	    newShareCount();
	}
	finalise(m_impl);
	decrementShareCount();
    }

    operator Tptr() {
	check();
	return m_impl;
    }

    operator const Tptr() const {
	return m_impl;
    }

protected:
//    LockingWrapperBase() :
//	m_impl(T::_nil()), m_implBackup(T::_nil()), m_shareCount(new int(1)) {
//	cout << "LockingWrapperBase ctor()" << endl;
//	checkLocal();
//    }

    LockingWrapperBase(Tptr t) :
	m_impl(t), m_implBackup(t), m_shareCount(new int(1)) {
	cout << "LockingWrapperBase ctor(" << (long)t << ")" << endl;
	checkLocal();
    }

    LockingWrapperBase(const LockingWrapperBase &w) :
 	m_impl(T::_duplicate(w.m_impl)), m_implBackup(m_impl),
	m_shareCount(w.m_shareCount) {
	checkLocal();
	incrementShareCount();
    }

    // The shareCount is to deal with the case of assigning one
    // wrapper to another: we don't want the resource locked until
    // both are out of scope.  The m_implBackup is for a different
    // reason: this can (in common with CORBA _var objects) be cast to
    // a _ptr for use as an inout parameter, and the called method
    // could modify it without our knowledge.  See MICO headers for a
    // similar thing in the _var world.

    LockingWrapperBase &operator=(const LockingWrapperBase &w) {
#ifdef DEBUG
 	cout << "Wrapper operator=" << endl;
#endif
	if (this != &w) {
	    reset(T::_duplicate(w.m_impl));
	    decrementShareCount();
	    m_shareCount = w.m_shareCount;
	    incrementShareCount();
	}
	return *this;
    }

    void check() {
	if (m_impl != m_implBackup) {
	    // we've been changed by an external agent!
	    finalise(m_implBackup);
	    newShareCount();
	    m_implBackup = m_impl;
	    checkLocal();
	}
    }

    void free() {
	check();
	finalise(m_impl);
	if (!CORBA::is_nil(m_impl)) m_implBackup = m_impl = T::_nil();
    }

    void reset(Tptr t) {
	free();
	m_implBackup = m_impl = t;
	checkLocal();
    }

    // release() works on nil
    virtual void finalise(Tptr o) {
	if (!CORBA::is_nil(o)) {
#ifdef DEBUG
	    cout << "Finalising some wrapped object" << endl;
#endif
	    CORBA::release(o);
	}
    }

    void delay(int delayCount);

    virtual Tptr impl() { return m_impl; }

    bool isShared() const { return (*m_shareCount) > 1; }
    void incrementShareCount() { ++*m_shareCount; }
    void decrementShareCount() { if (--*m_shareCount < 1) delete m_shareCount; }
    void newShareCount() { decrementShareCount(); m_shareCount = new int(1); }

    void checkLocal() {
	//!!! MICO-specific; when we move to the POA we should be able
	// to do this right
	m_local = (CORBA::is_nil(m_impl) ||
		   !(m_impl->_is_a_remote(m_impl->_repoid())));
#ifdef DEBUG
	cout << "wrapper: local " << m_local << " (impl is " << m_impl
	     << ")" << endl;
#endif
    }

    bool isLocal() { return m_local; }

    void checkTransaction(TransactionId id)
	throw (BadTransaction) {

	// The proper transaction checks are done on the server side,
	// but we do a quick check here.  Clients should not rely on
	// this check, because it obviously can't be done for any
	// class for which there are no wrappers.

	// Although the methods as defined in IDL allow you to pass a
	// TransactionId of NoTransaction, it's definitely a bad idea
	// to do so for a modifying operation from an out-of-process
	// client.  If you want to pass NoTransaction, these wrappers
	// don't want to be your friend.

	if (!isLocal()) {
	    if (id == NoTransaction) mico_throw(BadTransaction());
	}

	// On the other hand, in-process clients *cannot* carry out
	// undoable operations for threading reasons -- it's not
	// possible to block waiting for the transaction id.  For this
	// reason, an in-process client will be permitted to use a
	// transaction id of NoTransaction, which is what
	// Composition::startTransaction() will return if it finds a
	// lock is not immediately available for an in-process client.
	// Obviously this means most of the editing operations should
	// not be implemented in in-process clients, or else locking
	// integrity is compromised.
    }

protected:
    Tptr m_impl;
    Tptr m_implBackup;
    int *m_shareCount;
    bool m_local;
};

// }}}
// {{{ Standard wrapper methods

// The parent class's dtor calls finalise correctly, but it'll only
// get the parent class's finalise.  So we call finalise ourselves and
// then reset the impl pointers to nil so as to give the base class
// nothing to do except decrement the share count.

#define STANDARD_WRAPPER_METHODS(WrapperClass, WrappedClass, WrappedPtr) \
\
public: \
    ~WrapperClass() { \
	if (m_implBackup != m_impl) { \
	    finalise(m_implBackup); \
	    newShareCount(); \
	} \
	finalise(impl()); \
	m_implBackup = m_impl = WrappedClass::_nil(); \
    } \
\
public: \
    WrapperClass *operator->() { check(); return this; } // evil, but fun evil


// The amount of forwarding that goes on (on client and server
// sides) when using these methods simply doesn't bear thinking
// about.  But I'm going to think about it anyway.
// 
// If you call, say, deleteLeft() on a mutator wrapper on a
// non-collocated client, your call will first be forwarded to the
// client stub it wraps.  The stub will encode your request and
// send it to the skeleton at the server, which will unmarshal and
// dispatch it to the server implementation object.  This will
// forward it on to a PartIteratorBody object, which forwards to
// PartImpl's erase method.  PartImpl passes the buck to the
// RobustIteratorList it owns, which forwards it to the
// FastInsertVector concrete container class.  And then the
// forwarding chain unwinds again to bring back the return value.
//
// If you add a method to a PartIterator in the IDL, you also have
// to write a forwarder in the client wrapper class, a forwarder
// in the server skeleton class, and an implementation in the
// server's PartIteratorBody class.  Terse and elegant, huh?

// }}}
// {{{ Composition and Part

template <class T = Composition, class Tptr = Composition_ptr>
class CompositionWrapperT :
virtual public LockingWrapperBase<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(CompositionWrapperT, T, Tptr);

public:
    CompositionWrapperT(Tptr i = T::_nil()) :
	LockingWrapperBase<T, Tptr>(i) { }
    CompositionWrapperT(const CompositionWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w) { }

    virtual TransactionId startTransaction(CORBA::WChar *name,
					   PartBasicIterator_ptr leftExtent,
					   PartBasicIterator_ptr rightExtent)
	throw (IncompatibleIterators, NoSuchPart);

    virtual void endTransaction(TransactionId id)
	throw (BadTransaction)
    {
#ifdef DEBUG
	    cout << "Ending transaction" << endl;
#endif
	impl()->endTransaction(id);
    }

    virtual CORBA::WChar *getName() { return impl()->getName(); }
    virtual void setName(TransactionId id, CORBA::WChar *name)
	throw (BadTransaction) {
	checkTransaction(id); impl()->setName(id, name);
    }

    virtual CORBA::Short getPartCount() { return impl()->getPartCount(); }
    virtual Part_ptr getPart(CORBA::Short i) { return impl()->getPart(i); }

    virtual void createPart(TransactionId id, CORBA::Short index)
	throw (IndexOutOfRange, BadTransaction) {
#ifdef DEBUG
	    cout << "Creating part: id = " << id << ", index = " << index
		<< endl;
#endif
	checkTransaction(id); impl()->createPart(id, index);
    }

    virtual void deletePart(TransactionId id, Part_ptr p)
	throw (NoSuchPart, BadTransaction);

    virtual BarList_ptr getBarList() {
	return impl()->getBarList();
    }

    virtual GroupInfoFactory_ptr getGroupInfoFactory() {
	return impl()->getGroupInfoFactory();
    }

    virtual ElementFactory_ptr getElementFactory() {
	return impl()->getElementFactory();
    }

    virtual void dump() { impl()->dump(); }
};

typedef CompositionWrapperT<> CompositionWrapper;
 

/* don't need this yet

template <class T = Part, class Tptr = Part_ptr>
class PartWrapperT : 
    virtual public LockingWrapperBase<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(PartWrapperT, T, Tptr);

public:
    PartWrapperT(Tptr i = T::_nil()) : LockingWrapperBase(i) { }
    PartWrapperT(const PartWrapperT<T, Tptr> &w) : LockingWrapperBase(w) { }

};
*/

// }}}

// {{{ PartIterators

template <class T = PartBasicIterator, class Tptr = PartBasicIterator_ptr>
class PartBasicIteratorWrapperT :
    virtual public LockingWrapperBase<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(PartBasicIteratorWrapperT, T, Tptr);

public:
    PartBasicIteratorWrapperT(Tptr i = T::_nil()) :
	LockingWrapperBase<T, Tptr>(i) { } 
    PartBasicIteratorWrapperT(const PartBasicIteratorWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w) { } 

    virtual void releaseImpl() { impl()->releaseImpl(); }

    virtual long getIndex() { return impl()->getIndex(); }
    virtual Part_ptr getPart() { return impl()->getPart(); }

    virtual CORBA::Boolean samePartAs(Tptr i) {
	return impl()->samePartAs(i);
    }
    virtual CORBA::Boolean isBefore(Tptr i) {
	return impl()->isBefore(i);
    }
    virtual CORBA::Boolean isSameAs(Tptr i) {
	return impl()->isSameAs(i);
    }

    virtual CORBA::Boolean isOK() { return impl()->isOK(); }

    virtual void goTo(Tptr i) throw (IncompatibleIterators) {
	impl()->goTo(i);
    }

    virtual CORBA::Boolean atLeftEnd() { return impl()->atLeftEnd(); }
    virtual CORBA::Boolean atRightEnd() { return impl()->atRightEnd(); }

    virtual CORBA::Boolean goLeft() { return impl()->goLeft(); }
    virtual CORBA::Boolean goRight() { return impl()->goRight(); }

    // and these are the ones that the wrapper is here for -- the ones
    // that need locking:

    virtual Element_ptr getLeftElement();
    virtual Element_ptr getRightElement();

    virtual Element_ptr nextLeft();
    virtual Element_ptr nextRight();

    // we don't provide the explicitly locked alternatives

protected:
    virtual void finalise(Tptr o) {
	if (!CORBA::is_nil(o)) {
#ifdef DEBUG
	    cout << "Finalising iterator: about to releaseImpl()" << endl;
#endif
	    if (isShared()) {
#ifdef DEBUG
		cout << "(No! it's still shared)" << endl;
#endif
	    } else {
		o->releaseImpl();
	    }
	}
	LockingWrapperBase<T, Tptr>::finalise(o);
    }    
};

typedef PartBasicIteratorWrapperT<> PartBasicIteratorWrapper;


template <class T = PartTimedIterator, class Tptr = PartTimedIterator_ptr>
class PartTimedIteratorWrapperT :
    virtual public PartBasicIteratorWrapperT<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(PartTimedIteratorWrapperT, T, Tptr);

public:
    PartTimedIteratorWrapperT(Tptr i = T::_nil()) :
	LockingWrapperBase<T, Tptr>(i),
	PartBasicIteratorWrapperT<T, Tptr>(i) { }
    PartTimedIteratorWrapperT(const PartTimedIteratorWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w),
	PartBasicIteratorWrapperT<T, Tptr>(w) { }

    virtual void goToTime(Rosegarden::Time t) { impl()->goToTime(t); }

    virtual Rosegarden::Time getTimeFromStart() {
	return impl()->getTimeFromStart();
    }
    virtual Rosegarden::Time getTimeFromEnd() {
	return impl()->getTimeFromEnd();
    }

    virtual short getChordNoteIndex() { return impl()->getChordNoteIndex(); }
};

typedef PartTimedIteratorWrapperT<> PartTimedIteratorWrapper;
typedef PartTimedIteratorWrapperT<PartTimedNoteIterator,
				  PartTimedNoteIterator_ptr>
    PartTimedNoteIteratorWrapper;


template <class T = PartBasicMutator, class Tptr = PartBasicMutator_ptr>
class PartBasicMutatorWrapperT :
    virtual public PartBasicIteratorWrapperT<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(PartBasicMutatorWrapperT, T, Tptr);

public:
    PartBasicMutatorWrapperT(Tptr i = T::_nil()) :
	LockingWrapperBase<T, Tptr>(i),
	PartBasicIteratorWrapperT<T, Tptr>(i) { }
    PartBasicMutatorWrapperT(const PartBasicMutatorWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w),
	PartBasicIteratorWrapperT<T, Tptr>(w) { }

    virtual void insert(TransactionId id, Element_ptr e)
	throw (BadTransaction) {
	checkTransaction(id); impl()->insert(id, e);
    }

    virtual CORBA::Boolean deleteLeft(TransactionId id)
	throw (BadTransaction);
    virtual CORBA::Boolean deleteRight(TransactionId id)
	throw (BadTransaction);
};

typedef PartBasicMutatorWrapperT<> PartBasicMutatorWrapper;


template <class T = PartTimedMutator, class Tptr = PartTimedMutator_ptr>
class PartTimedMutatorWrapperT :
    virtual public PartBasicMutatorWrapperT<T, Tptr>,
    virtual public PartTimedIteratorWrapperT<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(PartTimedMutatorWrapperT, T, Tptr);

public:
    PartTimedMutatorWrapperT(Tptr i = T::_nil()) :
	LockingWrapperBase<T, Tptr>(i),
	PartBasicIteratorWrapperT<T, Tptr>(i),
	PartBasicMutatorWrapperT<T, Tptr>(i),
	PartTimedIteratorWrapperT<T, Tptr>(i) { }
    PartTimedMutatorWrapperT(const PartTimedMutatorWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w),
	PartBasicIteratorWrapperT<T, Tptr>(w),
	PartBasicMutatorWrapperT<T, Tptr>(w),
	PartTimedIteratorWrapperT<T, Tptr>(w) { }

    virtual void overlay(TransactionId id, Element_ptr e)
        throw (BadTransaction) {
	checkTransaction(id); impl()->overlay(id, e);
    }
    virtual void overlayAtTime(TransactionId id, Element_ptr e, Rosegarden::Time t)
        throw (BadTransaction) {
	checkTransaction(id); impl()->overlayAtTime(id, e, t);
    }
};

typedef PartTimedMutatorWrapperT<> PartTimedMutatorWrapper;
typedef PartTimedMutatorWrapperT<PartTimedNoteMutator,
				 PartTimedNoteMutator_ptr>
    PartTimedNoteMutatorWrapper;

// }}}
// {{{ Elements

template <class T = Element, class Tptr = Element_ptr>
class ElementWrapperT :
    virtual public LockingWrapperBase<T, Tptr>
{
    STANDARD_WRAPPER_METHODS(ElementWrapperT, T, Tptr);

public:
    ElementWrapperT(Tptr i = T::_nil()) : LockingWrapperBase<T, Tptr>(i) { }
    ElementWrapperT(const ElementWrapperT<T, Tptr> &w) :
	LockingWrapperBase<T, Tptr>(w) { }

    virtual void releaseImpl() { impl()->releaseImpl(); }

    virtual Rosegarden::Time getDuration() { return impl()->getDuration(); }

protected:
    virtual void finalise(Tptr o) {
	if (!CORBA::is_nil(o)) {
#ifdef DEBUG
	    cout << "Finalising Element: about to unlock()" << endl;
#endif
	    if (isShared()) {
#ifdef DEBUG
		cout << "(No! it's still shared)" << endl;
#endif
	    } else if (isLocal()) {
#ifdef DEBUG
		cout << "(No! It's local)" << endl;
#endif
	    } else {
		o->unlock();
	    }
	}
	LockingWrapperBase<T, Tptr>::finalise(o);
    }
};

typedef ElementWrapperT<> ElementWrapper;    

// }}}
// {{{ Transaction

class Transaction
{
public:
    Transaction(CompositionWrapper &c, my_wstring name,
		PartBasicIterator_ptr leftExtent,
		PartBasicIterator_ptr rightExtent) :
	m_cw(c) {
	m_id = m_cw->startTransaction(CORBA::wstring_dup(name.c_str()),
				      leftExtent, rightExtent);
#ifdef DEBUG
	cout << "Made a new Transaction object, id " << m_id << endl;
#endif
	m_ended = false;
    }
    virtual ~Transaction() {
	// if this assertion fails, you forgot to call end()
	if (!m_ended) {
	    cerr << "Warning: Transaction wrapper for id " << m_id
		 << " deleted without end() called" << endl;
	}
    }
    void end() { 
	if (m_cw) m_cw->endTransaction(m_id);
#ifdef DEBUG
	cout << "Ending Transaction object, id " << m_id << endl;
#endif
	m_ended = true;
    }

    operator TransactionId() {
#ifdef DEBUG
	cout << "Casting transaction " << this << " to TransactionId " << m_id
	    << endl;
#endif
	return m_id; }

private:
    // not provided:
    Transaction(const Transaction &t);
    Transaction &operator=(const Transaction &t);

    CompositionWrapper &m_cw;
    TransactionId m_id;
    bool m_ended;
};

// }}}

} // end of Server namespace
} // end of Rosegarden namespace

#endif

#include "RosegardenServerWrapper.cxx"
