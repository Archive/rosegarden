// -*- c-file-style:  "bsd" -*-
#ifndef _ROSEGARDEN_SERVER_WRAPPER_CXX_
#define _ROSEGARDEN_SERVER_WRAPPER_CXX_

#include "RosegardenServerWrapper.h"

#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

namespace Rosegarden {
namespace Server {

template <class T, class Tptr>
void LockingWrapperBase<T, Tptr>::delay(int delayCount)
{
    static bool seeded(false);
    if (!seeded) {
	//!!! it'd be better to know it's already been done somewhere else:
	srand((unsigned int)getpid());
	seeded = true;
    }

    if (isLocal()) {
	cerr << "Warning: LockingWrapperBase::delay() called on wrapper for"
	     << "\nlocal object; this is probably bad news" << endl;
    }

    // very very naive for now
    struct timeval timeout;

    long delay = 40 + ((rand() >> 4) % 30); // ms
    int dc = delayCount;

    while (dc > 0) {
	delay = (long)(delay * 1.2);
	if (delay > 3e4) break;
	--dc;
    }

    timeout.tv_sec = delay / 1000;
    timeout.tv_usec = (delay % 1000) * 1000;

    cout << "Delaying for " << ((double)delay / 1000.0) << " sec "
	 << "(delayCount = " << delayCount << ")" << endl;

    select(0, NULL, NULL, NULL, &timeout);
}

#define WAIT_FOR_LOCK(INVARIANT_GUARANTEE, LOCKING_CALL) \
{ \
    CORBA::Boolean haveLock = false; \
    int dc = 0; \
\
    while (!haveLock) { \
	INVARIANT_GUARANTEE; \
	haveLock = LOCKING_CALL; \
	if (!haveLock) delay(dc++); \
    } \
}

//!!! Need to handle OBJECT_NOT_EXIST exceptions appropriately throughout

template <class T, class Tptr>
TransactionId CompositionWrapperT<T, Tptr>::startTransaction
	(CORBA::WChar *name,
	 PartBasicIterator_ptr leftExtent,
	 PartBasicIterator_ptr rightExtent)
throw (IncompatibleIterators, NoSuchPart)
{
    TransactionId id;

    if (isLocal()) { return NoTransaction; } // see comment in LockingWrapperBase::checkTransaction()

#ifdef DEBUG
    cout << "Trying for lock for transaction \"" << my_wstring(CORBA::wstring_dup(name)) << "\"..." << endl;
#endif
    WAIT_FOR_LOCK( , impl()->startTransaction(name, id, leftExtent, rightExtent));
#ifdef DEBUG
    cout << "...success (id is " << id << ")" << endl;
#endif
    return id;
}

template <class T, class Tptr>
void CompositionWrapperT<T, Tptr>::deletePart(TransactionId id, Part_ptr p)
throw (NoSuchPart, BadTransaction)
{
    checkTransaction(id);
    if (isLocal()) return m_impl->deletePart(id, p);
    WAIT_FOR_LOCK( , impl()->deletePartLocked(id, p));
}

template <class T, class Tptr>
Element_ptr PartBasicIteratorWrapperT<T, Tptr>::getLeftElement()
{
    if (isLocal()) return m_impl->getLeftElement();
    Element_ptr e = Element::_nil();
    WAIT_FOR_LOCK(CORBA::release(e), impl()->getLeftElementLocked(e));
    return e; // hmm... client still has to unlock, unless we return a wrapper
}

template <class T, class Tptr>
Element_ptr PartBasicIteratorWrapperT<T, Tptr>::getRightElement()
{
    if (isLocal()) return m_impl->getRightElement();
    Element_ptr e = Element::_nil();
    WAIT_FOR_LOCK(CORBA::release(e), impl()->getRightElementLocked(e));
    return e; // hmm... client still has to unlock, unless we return a wrapper
}

template <class T, class Tptr>
Element_ptr PartBasicIteratorWrapperT<T, Tptr>::nextLeft()
{
    if (isLocal()) return m_impl->nextLeft();
    Element_ptr e = Element::_nil();
    WAIT_FOR_LOCK(CORBA::release(e), impl()->nextLeftLocked(e));
    return e; // hmm... client still has to unlock, unless we return a wrapper
}

template <class T, class Tptr>
Element_ptr PartBasicIteratorWrapperT<T, Tptr>::nextRight()
{
    if (isLocal()) return m_impl->nextRight();
    Element_ptr e = Element::_nil();
    WAIT_FOR_LOCK(CORBA::release(e), impl()->nextRightLocked(e));
    return e; // hmm... client still has to unlock, unless we return a wrapper
}

template <class T, class Tptr>
CORBA::Boolean PartBasicMutatorWrapperT<T, Tptr>::deleteLeft(TransactionId id)
throw (BadTransaction)
{
    checkTransaction(id);
    if (isLocal()) return m_impl->deleteLeft(id);
    CORBA::Boolean b;
    WAIT_FOR_LOCK( , impl()->deleteLeftLocked(id, b));
    return b;
}

template <class T, class Tptr>
CORBA::Boolean PartBasicMutatorWrapperT<T, Tptr>::deleteRight(TransactionId id)
throw (BadTransaction)
{
    checkTransaction(id);
    if (isLocal()) return m_impl->deleteRight(id);
    CORBA::Boolean b;
    WAIT_FOR_LOCK( , impl()->deleteRightLocked(id, b));
    return b;
}

} // end of Server namespace
} // end of Rosegarden namespace

#endif

