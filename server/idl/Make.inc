
clean:	server-idl-clean
server-idl-clean:
	rm -f server/idl/*.[Ch]

SERVER_GENHEADERS	= $(patsubst %.idl,%.h,$(shell ls server/idl/*.idl))
GENHEADERS	+= $(SERVER_GENHEADERS)

server/idl/+objects/Make.gen: $(SERVER_GENHEADERS)

