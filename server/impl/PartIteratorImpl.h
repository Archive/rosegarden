// -*- c-file-style:  "bsd" -*-
#ifndef _PART_ITERATOR_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include "PartIteratorBody.h"
#include "PartImpl.h"
#include "Transaction.h"

using namespace Rosegarden;
using namespace Rosegarden::Server;

class PartBasicIteratorImpl : virtual public PartBasicIterator_skel
{
public:
    PartBasicIteratorImpl(PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = false) :
	m_body(pi, itr, notesOnly) { }
    PartBasicIteratorImpl(const PartIteratorBody &b) :
	m_body(b) { }
    virtual ~PartBasicIteratorImpl() { }

    virtual void releaseImpl() {
	cout << "Iterator releaseImpl() called" << endl;
	CORBA::release(this);
    }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartBasicIteratorImpl *copy = new PartBasicIteratorImpl(m_body);
	return copy->_this();
    }

    virtual long getIndex() { return m_body.getIndex(); }
    virtual Part_ptr getPart() { return m_body.getPart(); }

    virtual CORBA::Boolean samePartAs(PartBasicIterator_ptr i) {
	return m_body.samePartAs(i);
    }
    virtual CORBA::Boolean isBefore(PartBasicIterator_ptr i) {
	return m_body.isBefore(i);
    }
    virtual CORBA::Boolean isSameAs(PartBasicIterator_ptr i) {
	return m_body.isSameAs(i);
    }

    virtual CORBA::Boolean isOK() { return m_body.isOK(); }

    virtual void goTo(PartBasicIterator_ptr i)
	throw (IncompatibleIterators) {
	m_body.goTo(i);
    }

    virtual CORBA::Boolean atLeftEnd() { return m_body.atLeftEnd(); }
    virtual CORBA::Boolean atRightEnd() { return m_body.atRightEnd(); }

    virtual CORBA::Boolean goLeft() { return m_body.goLeft(); }
    virtual CORBA::Boolean goRight() { return m_body.goRight(); }

    virtual Element_ptr getLeftElement() { return m_body.getLeftElement(); }
    virtual Element_ptr getRightElement() { return m_body.getRightElement(); }

    virtual Element_ptr nextLeft() { return m_body.nextLeft(); }
    virtual Element_ptr nextRight() { return m_body.nextRight(); }

    virtual CORBA::Boolean getLeftElementLocked(Element_ptr &e) {
	return m_body.getLeftElementLocked(e);
    }
    virtual CORBA::Boolean getRightElementLocked(Element_ptr &e) {
	return m_body.getRightElementLocked(e);
    }

    virtual CORBA::Boolean nextLeftLocked(Element_ptr &e) {
	return m_body.nextLeftLocked(e);
    }
    virtual CORBA::Boolean nextRightLocked(Element_ptr &e) {
	return m_body.nextRightLocked(e);
    }
    
protected:
    PartIteratorBody m_body;
};

class PartBasicMutatorImpl : virtual public PartBasicIteratorImpl,
			     virtual public PartBasicMutator_skel,
			     virtual public Transactor
{
public:
    PartBasicMutatorImpl(Transactable *t,
			 PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = false) :
	PartBasicIteratorImpl(pi, itr, notesOnly),
	Transactor(t) { }
    PartBasicMutatorImpl(const PartIteratorBody &b, Transactable *t) :
	PartBasicIteratorImpl(b),
	Transactor(t) { }
    virtual ~PartBasicMutatorImpl() { }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartBasicMutatorImpl *copy =
	    new PartBasicMutatorImpl(m_body, getTransactable());
	return copy->_this();
    }

    virtual void insert(TransactionId id, Element_ptr e)
	throw (BadTransaction) {
	checkGood(id); m_body.insert(e);
    }
    virtual CORBA::Boolean deleteLeft(TransactionId id)
	throw (BadTransaction) {
	checkGood(id); return m_body.deleteLeft();
    }
    virtual CORBA::Boolean deleteRight(TransactionId id)
	throw (BadTransaction) {
	checkGood(id); return m_body.deleteRight();
    }
    virtual CORBA::Boolean deleteLeftLocked
    (TransactionId id, CORBA::Boolean &d)
	throw (BadTransaction) {
	checkGood(id); return m_body.deleteLeftLocked(d);
    }
    virtual CORBA::Boolean deleteRightLocked
    (TransactionId id, CORBA::Boolean &d)
	throw (BadTransaction) {
	checkGood(id); return m_body.deleteRightLocked(d);
    }

    // resolve multiple inheritance ambiguity
    PartBasicMutator_ptr _this() { return PartBasicMutator_skel::_this(); }
};

class PartTimedIteratorImpl : virtual public PartBasicIteratorImpl,
			      virtual public PartTimedIterator_skel
{
public:
    PartTimedIteratorImpl(PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = false) :
	PartBasicIteratorImpl(pi, itr, notesOnly) { }
    PartTimedIteratorImpl(const PartIteratorBody &b) :
	PartBasicIteratorImpl(b) { }
    virtual ~PartTimedIteratorImpl() { }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartTimedIteratorImpl *copy = new PartTimedIteratorImpl(m_body);
	return copy->_this();
    }

    virtual void goToTime(Time t) throw (InvalidTime) { m_body.goToTime(t); }
    virtual Time getTimeFromStart() { return m_body.getTimeFromStart(); }
    virtual Time getTimeFromEnd() { return m_body.getTimeFromEnd(); }
    virtual CORBA::Short getChordNoteIndex() { return m_body.getChordNoteIndex(); }

    // resolve multiple inheritance ambiguity
    PartTimedIterator_ptr _this() { return PartTimedIterator_skel::_this(); }
};

class PartTimedMutatorImpl : virtual public PartTimedIteratorImpl,
			     virtual public PartBasicMutatorImpl,
			     virtual public PartTimedMutator_skel
{
public:
    PartTimedMutatorImpl(Transactable *t,
			 PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = false) :
	PartBasicIteratorImpl(pi, itr, notesOnly),
	PartBasicMutatorImpl(t, pi, itr, notesOnly),
	PartTimedIteratorImpl(pi, itr, notesOnly),
	Transactor(t) { }
    PartTimedMutatorImpl(const PartIteratorBody &b, Transactable *t) :
	PartBasicIteratorImpl(b),
	PartBasicMutatorImpl(b, t),
	PartTimedIteratorImpl(b),
	Transactor(t) { }
    virtual ~PartTimedMutatorImpl() { }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartTimedMutatorImpl *copy =
	    new PartTimedMutatorImpl(m_body, getTransactable());
	return copy->_this();
    }

    virtual void overlay(TransactionId id, Element_ptr e)
	throw (BadTransaction) {
	checkGood(id); m_body.overlay(e);
    }
    virtual void overlayAtTime(TransactionId id, Element_ptr e, Time t)
	throw (BadTransaction) {
	checkGood(id); m_body.overlayAtTime(e, t);
    }

    virtual CORBA::Boolean stripLeft(TransactionId id)
	throw (BadTransaction) {
	checkGood(id); return m_body.stripLeft();
    }
    virtual CORBA::Boolean stripRight(TransactionId id)
	throw (BadTransaction) {
	checkGood(id); return m_body.stripRight();
    }

    virtual CORBA::Boolean stripLeftLocked(TransactionId id, CORBA::Boolean &b)
	throw (BadTransaction) {
	checkGood(id); return m_body.stripLeftLocked(b);
    }
    virtual CORBA::Boolean stripRightLocked(TransactionId id, CORBA::Boolean &b)
	throw (BadTransaction) {
	checkGood(id); return m_body.stripRightLocked(b);
    }

    // resolve multiple inheritance ambiguity
    PartTimedMutator_ptr _this() { return PartTimedMutator_skel::_this(); }
};

class PartTimedNoteIteratorImpl : virtual public PartTimedIteratorImpl,
				  virtual public PartTimedNoteIterator_skel
{
public:
    PartTimedNoteIteratorImpl(PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = true) :
	PartBasicIteratorImpl(pi, itr, notesOnly),
	PartTimedIteratorImpl(pi, itr, notesOnly) { }
    PartTimedNoteIteratorImpl(const PartIteratorBody &b) :
	PartBasicIteratorImpl(b),
	PartTimedIteratorImpl(b) { }
    virtual ~PartTimedNoteIteratorImpl() { }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartTimedNoteIteratorImpl *copy = new PartTimedNoteIteratorImpl(m_body);
	return copy->_this();
    }

    // resolve multiple inheritance ambiguity
    PartTimedNoteIterator_ptr _this() {
	return PartTimedNoteIterator_skel::_this();
    }
};

class PartTimedNoteMutatorImpl : virtual public PartTimedMutatorImpl,
				 virtual public PartTimedNoteMutator_skel
{
public:
    PartTimedNoteMutatorImpl(Transactable *t,
			     PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly = true) :
	PartBasicIteratorImpl(pi, itr, notesOnly),
	PartBasicMutatorImpl(t, pi, itr, notesOnly),
	PartTimedIteratorImpl(pi, itr, notesOnly),
	PartTimedMutatorImpl(t, pi, itr, notesOnly),
	Transactor(t) { }
    PartTimedNoteMutatorImpl(const PartIteratorBody &b, Transactable *t) :
	PartBasicIteratorImpl(b),
	PartBasicMutatorImpl(b, t),
	PartTimedIteratorImpl(b),
	PartTimedMutatorImpl(b, t),
	Transactor(t) { }
    virtual ~PartTimedNoteMutatorImpl() { }

    virtual PartBasicIterator_ptr clone() {
	if (!isOK()) return PartBasicIterator::_nil();
	PartTimedNoteMutatorImpl *copy =
	    new PartTimedNoteMutatorImpl(m_body, getTransactable());
	return copy->_this();
    }

    // resolve multiple inheritance ambiguity
    PartTimedNoteMutator_ptr _this() {
	return PartTimedNoteMutator_skel::_this();
    }
};

#endif
