// -*- c-file-style:  "bsd" -*-
#ifndef _COMPOSITION_IMPL_H_
#define _COMPOSITION_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include <common/my_wstring.h>
#include "Transaction.h"

using namespace Rosegarden;
using namespace Rosegarden::Server;
class RosegardenServerImpl;
class PartImpl;
class BarListImpl;

class CompositionImpl : virtual public Composition_skel,
			virtual public Transactable,
			virtual public Transactor
{
public:
    CompositionImpl(RosegardenServerImpl *server);
    virtual ~CompositionImpl();

    virtual CORBA::Boolean startTransaction(const CORBA::WChar *n, TransactionId &,
					    PartBasicIterator_ptr leftExtent,
					    PartBasicIterator_ptr rightExtent)
	throw (NoSuchPart, IncompatibleIterators);

    virtual void endTransaction(TransactionId id)
	throw (BadTransaction);
    
    virtual void setName(TransactionId id, const CORBA::WChar *name)
	throw (BadTransaction);

    virtual CORBA::WChar *getName() {
	return CORBA::wstring_dup(m_name.c_str());
    }
    
    virtual CORBA::Short getPartCount() {
	return m_parts.size();
    }

    virtual Part_ptr getPart(CORBA::Short pos)
	throw (IndexOutOfRange);
    virtual void createPart(TransactionId id, CORBA::Short pos)
	throw (IndexOutOfRange, BadTransaction);
    virtual void deletePart(TransactionId id, Part_ptr part)
	throw (NoSuchPart, BadTransaction);
    virtual CORBA::Boolean deletePartLocked(TransactionId id, Part_ptr part)
	throw (NoSuchPart, BadTransaction);

    virtual BarList_ptr getBarList();
    virtual GroupInfoFactory_ptr getGroupInfoFactory();
    virtual ElementFactory_ptr getElementFactory();

    // for call by other library objects
    virtual void registerObserver(CompositionObserver_ptr);
    virtual void unregisterObserver(CompositionObserver_ptr);

    virtual void dump(); // for debugging

private:
    my_wstring m_name;

    vector<PartImpl *> m_parts;
    vector<PartImpl *>::iterator locate(Part_ptr) throw (NoSuchPart);
    BarListImpl *m_barList;

    // Based on Rosegarden 2.1 editor/src/Undo.c.  This is very
    // sketchy.  The main thing is to get the necessary information
    // passed through the API; making Undo actually *work* is very
    // much second on the list

    // Presently I'm thinking the elements in an UndoSection should be
    // cloned from the originals (i.e. not just shared with an
    // additional CORBA refcount).  Much easier to reason with

    // Of course, even easier would be if the elements were structs
    // rather than interfaces (pure data in IDL).  There are practical
    // problems with that too, of course

    struct UndoSection
    {
	long startIndex;
	long endIndex;
	vector<Element_ptr> elements;
    };

    //!!! Oops!  What about createPart?

    struct UndoRec
    {
	// If some elements within a Part have been changed, the
	// before and after records indicate the extent and contents
	// of the changed area before and, indeed, after the change.
	// If on the other hand a whole Part has been deleted,
	// partDeleted will be true, and then the index shows where
	// that Part used to live, the before record shows what it
	// contained, and the after record is undefined.

	short partIndex;
	bool partDeleted;

	UndoSection before; // elements needed for undo, extents for redo
	UndoSection after; // elements needed for redo, extents for undo
    };

    struct UndoStack
    {
	vector<UndoRec> undos;
	vector<UndoRec>::iterator nextUndo; // nextRedo should be nextUndo+1
    };

    UndoStack m_undoStack;

    struct PendingUndo
    {
	short partIndex;
	bool partDeleted;

	long startIndex;
	PartBasicIterator_ptr end;
	vector<Element_ptr> savedElements; // to be copied to before.elements
    };

    PendingUndo m_pendingUndo;

    RosegardenServerImpl *m_server;
    GroupInfoFactory_ptr m_groupInfoFactory;
    ElementFactory_ptr m_elementFactory;

#ifdef NOT_DEFINED
    struct NotifyErase // functor
    {
	NotifyErase(CompositionImpl *ci, int pos, Part_ptr p) :
	    m_ci(ci), m_pos(pos), m_p(p) { }
	void operator()(CompositionObserver *i);
	CompositionImpl *m_ci;
	int m_pos;
	Part_ptr m_p;
    };

    struct NotifyInsert // functor
    {
	NotifyInsert(CompositionImpl *ci, int pos, Part_ptr p) :
	    m_ci(ci), m_pos(pos), m_p(p) { }
	void operator()(CompositionObserver *i);
	CompositionImpl *m_ci;
	int m_pos;
	Part_ptr m_p;
    };
#endif

    vector<CompositionObserver_ptr> m_observers;
    void callJustInsertedPartCallbacks(int pos, Part_ptr p);
    void callAboutToErasePartCallbacks(int pos, Part_ptr p);
};

#endif

