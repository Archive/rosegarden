// -*- c-file-style:  "bsd" -*-
#ifndef _ELEMENT_IMPL_H_
#define _ELEMENT_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include <string>
#include <vector>
#include <common/my_wstring.h>
#include <common/Lockable.h>
#include "CorbaCloner.h"
#include "Transaction.h"

using namespace Rosegarden;
using namespace Rosegarden::Server;

class ElementImpl : virtual public Element_skel,
		    virtual public SimpleLockable<>,
		    virtual public Transactor
{
public:
    ElementImpl(Transactable *t);
    virtual ~ElementImpl();

    virtual void releaseImpl();

    // need some timing info (for elements next to one another in a
    // list)

    virtual GroupInfo_ptr getGroupInfo();
    virtual void setGroupInfo(TransactionId id, GroupInfo_ptr g)
    throw (BadTransaction);

    virtual Time getDuration() { return 0; }

    virtual CORBA::Boolean lock() { return SimpleLockable<>::l_lock(); }
    virtual void unlock() { SimpleLockable<>::l_unlock(); }
    virtual CORBA::Boolean isLocked() { return SimpleLockable<>::l_isLocked(); }

private:
    GroupInfo_ptr m_groupInfo;	// I own this
};

class NoteImpl :
    virtual public ElementImpl, virtual public Note_skel
{
public:
    // oh, curse virtual inheritance for requiring all these constructors
    NoteImpl(Transactable *t, Time duration) :
	Transactor(t), ElementImpl(t), m_duration(duration) { }
    virtual ~NoteImpl() { }
    
    virtual Time getDuration() { return m_duration; }

    // No -- duration can only be set in the factory; changing it
    // would invalidate timings on TimedIterators (which get no
    // feedback for duration change, only for insert/delete)
//    virtual void setDuration(TransactionId id, Time value)
//	throw (BadTransaction) {
//	checkGood(id); m_duration = value;
//    }

    virtual Pitch getPitch() { return m_pitch; }
    virtual void setPitch(TransactionId id, Pitch value)
	throw (BadTransaction) {
	checkGood(id); m_pitch = value;
    }

    virtual Accidental getAccidental() { return m_accidental; }
    virtual void setAccidental(TransactionId id, Accidental value)
	throw (BadTransaction) {
	checkGood(id); m_accidental = value;
    }

    virtual Velocity getVelocity() { return m_velocity; }
    virtual void setVelocity(TransactionId id, Velocity v)
	throw (BadTransaction) {
	checkGood(id); m_velocity = v;
    }

    virtual void visit(ElementVisitor_ptr ev) {
	Note_var note(_this());
	ev->visitNote(note);
    }

    // resolve multiple inheritance ambiguity
    Note_ptr _this() { return Note_skel::_this(); }

private:
    Time m_duration;
    Pitch m_pitch;
    Accidental m_accidental;
    Velocity m_velocity;
};

//!!! must check if the Notes are deleted correctly from within the
//Chords  [[yes, I believe they are]]

class ChordImpl : virtual public ElementImpl, virtual public Chord_skel
{
private:
    // let's store the interface pointers internally -- only a good
    // idea if they were created on the server end, with a Factory;
    // we reserve the right to change our minds later
    typedef CloningWrapper<Note_ptr,
	CorbaCloner<Note, Note_ptr> > NoteWrapper;
    typedef vector<NoteWrapper> NoteWrapperList;

public:
    ChordImpl(Transactable *t, Time duration);
    virtual ~ChordImpl();

    virtual NoteList *getNotes();
    virtual void setNotes(TransactionId id, const NoteList &value)
	throw (InsufficientData, BadTransaction);

    virtual CORBA::Short getNoteCount();
    virtual Note_ptr getNote(CORBA::Short index) throw (IndexOutOfRange);

    virtual Time getDuration();

    virtual CORBA::WChar *getName();
    virtual void setName(TransactionId id, const CORBA::WChar *name)
	throw (BadTransaction);

    virtual CORBA::Boolean isNamed();
    virtual void setNamed(TransactionId id, CORBA::Boolean value)
	throw (BadTransaction);

    virtual void visit(ElementVisitor_ptr ev) {
	Chord_var chord(_this());
	ev->visitChord(chord);
    }

    // resolve multiple inheritance ambiguity
    Chord_ptr _this() { return Chord_skel::_this(); }

private:
    NoteWrapperList m_notes;	// I own the note pointers
    my_wstring m_name;
    Time m_duration;
    bool m_named;
};

class RestImpl : virtual public ElementImpl, virtual public Rest_skel
{
public:
    RestImpl(Transactable *t, Time duration) :
	Transactor(t), ElementImpl(t), m_duration(duration) { }
    virtual ~RestImpl() { }

    virtual Time getDuration() { return m_duration; }

    // No.  Duration can only be set in the factory, to avoid
    // invalidating the timings of TimedIterators (which don't get
    // feedback for duration changes, only for insert/delete)
//    virtual void setDuration(TransactionId id, Time d)
//        throw (BadTransaction) {
//	checkGood(id); m_duration = d;
//    }

    virtual void visit(ElementVisitor_ptr ev) {
	Rest_var rest(_this());
	ev->visitRest(rest);
    }

    // resolve multiple inheritance ambiguity
    Rest_ptr _this() { return Rest_skel::_this(); }

private:
    Time m_duration;
};

class TempoChangeImpl : virtual public ElementImpl, virtual public TempoChange_skel
{
public:
    TempoChangeImpl(Transactable *t) : Transactor(t), ElementImpl(t) { }
    virtual ~TempoChangeImpl() { }

    virtual Time getBeatLength() { return m_beatLength; }
    virtual void setBeatLength(TransactionId id, Time b)
        throw (BadTransaction) {
	checkGood(id); m_beatLength = b;
    }

    virtual CORBA::Short getBPM() { return m_bpm; }
    virtual void setBPM(TransactionId id, CORBA::Short b)
        throw (BadTransaction) {
	checkGood(id); m_bpm = b;
    }

    virtual void visit(ElementVisitor_ptr ev) {
	TempoChange_var tc(_this());
	ev->visitTempoChange(tc);
    }

    // resolve multiple inheritance ambiguity
    TempoChange_ptr _this() { return TempoChange_skel::_this(); }

private:
    Time m_beatLength;
    CORBA::Short m_bpm;
};

class TextImpl : virtual public ElementImpl, virtual public Text_skel
{
public:
    TextImpl(Transactable *t) : Transactor(t), ElementImpl(t) { }
    virtual ~TextImpl() { }

    virtual TextType getType() { return m_type; }
    virtual void setType(TransactionId id, TextType t)
        throw (BadTransaction) {
	checkGood(id); m_type = t;
    }

    virtual CORBA::WChar *getText() {
	return CORBA::wstring_dup(m_content.c_str());
    }
    virtual void setText(TransactionId id, const CORBA::WChar *content)
        throw (BadTransaction) {
	checkGood(id); m_content = content;
    }

    virtual void visit(ElementVisitor_ptr ev) {
	Text_var text(_this());
	ev->visitText(text);
    }

    // resolve multiple inheritance ambiguity
    Text_ptr _this() { return Text_skel::_this(); }

private:
    TextType m_type;
    my_wstring m_content;
};

class ClefChangeImpl : virtual public ElementImpl, virtual public ClefChange_skel
{
public:
    ClefChangeImpl(Transactable *t) : Transactor(t), ElementImpl(t) { }
    virtual ~ClefChangeImpl() { }

    virtual Clef getClef() { return m_type; }
    virtual void setClef(TransactionId id, Clef c)
        throw (BadTransaction) {
	checkGood(id); m_type = c;
    }

    virtual Pitch getTransposition() { return m_transposition; }
    virtual void setTransposition(TransactionId id, Pitch p)
        throw (BadTransaction) {
	checkGood(id); m_transposition = p;
    }

    virtual void visit(ElementVisitor_ptr ev) {
	ClefChange_var cc(_this());
	ev->visitClefChange(cc);
    }

    // resolve multiple inheritance ambiguity
    ClefChange_ptr _this() { return ClefChange_skel::_this(); }

private:
    Clef m_type;
    Pitch m_transposition;
};

class KeyChangeImpl : virtual public ElementImpl, virtual public KeyChange_skel
{
public:
    KeyChangeImpl(Transactable *t) : Transactor(t), ElementImpl(t) { }
    virtual ~KeyChangeImpl() { }

    virtual Key getKey() { return m_type; }
    virtual void setKey(TransactionId id, Key k)
        throw (BadTransaction) {
	checkGood(id); m_type = k;
    }

    virtual void getSharpsAndFlats(CORBA::Short &sharps, CORBA::Short &flats);

    virtual void visit(ElementVisitor_ptr ev) {
	KeyChange_var kc(_this());
	ev->visitKeyChange(kc);
    }

    // resolve multiple inheritance ambiguity
    KeyChange_ptr _this() { return KeyChange_skel::_this(); }

private:
    Key m_type;
};

class UnparsedDataImpl : virtual public ElementImpl, virtual public UnparsedData_skel
{
public:
    UnparsedDataImpl(Transactable *t) : Transactor(t), ElementImpl(t) { }
    virtual ~UnparsedDataImpl() { }

    virtual OctetSequence *getData();
    virtual void setData(TransactionId id, const OctetSequence &data)
	throw (BadTransaction);

    virtual void visit(ElementVisitor_ptr ev) {
	UnparsedData_var upd(_this());
	return ev->visitUnparsedData(upd);
    }

    // resolve multiple inheritance ambiguity
    UnparsedData_ptr _this() { return UnparsedData_skel::_this(); }

private:
    vector<unsigned char> m_data;
};

class ElementFactoryImpl : virtual public ElementFactory_skel, virtual public Transactor
{
public:
    ElementFactoryImpl(Transactable *t) : Transactor(t) { }
    virtual ~ElementFactoryImpl() { }

    virtual Note_ptr newNote(Time duration, Pitch pitch,
			     Accidental accidental, Velocity velocity);
    virtual void insertNote(TransactionId id, PartBasicMutator_ptr mutator, Time duration,
			    Pitch pitch, Accidental accidental, Velocity velocity)
	throw (BadTransaction);

    virtual Chord_ptr newChord(Time duration, const NoteList &notes);
    virtual void insertChord(TransactionId id, PartBasicMutator_ptr mutator, Time duration, const NoteList &notes)
	throw (BadTransaction);

    virtual Rest_ptr newRest(Time duration);
    virtual void insertRest(TransactionId id, PartBasicMutator_ptr mutator, Time duration)
	throw (BadTransaction);

    virtual TempoChange_ptr newTempoChange(Time beat, CORBA::Short bpm);
    virtual void insertTempoChange(TransactionId id, PartBasicMutator_ptr mutator, Time beat, CORBA::Short bpm)
	throw (BadTransaction);

    virtual Text_ptr newText(TextType type, const CORBA::WChar *content);
    virtual void insertText(TransactionId id, PartBasicMutator_ptr mutator, TextType type, const CORBA::WChar *content)
	throw (BadTransaction);

    virtual ClefChange_ptr newClefChange(Clef clef, Pitch transposition);
    virtual void insertClefChange(TransactionId id, PartBasicMutator_ptr mutator, Clef clef, Pitch transposition)
	throw (BadTransaction);

    virtual KeyChange_ptr newKeyChange(Key key);
    virtual void insertKeyChange(TransactionId id, PartBasicMutator_ptr mutator, Key key)
	throw (BadTransaction);
};
    

#endif

