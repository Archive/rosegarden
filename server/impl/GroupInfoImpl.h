// -*- c-file-style:  "bsd" -*-
#ifndef _GROUPINFO_IMPL_H_
#define _GROUPINFO_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include "Transaction.h"

using namespace Rosegarden;
using namespace Rosegarden::Server;

class GroupInfoImpl : virtual public GroupInfo_skel
{
public:
    virtual ~GroupInfoImpl() { }

    virtual CORBA::Boolean isAtStart() { return m_atStart; }
    virtual CORBA::Boolean isAtEnd() { return m_atStart; }

protected:
    GroupInfoImpl(bool atStart, bool atEnd) :
	m_atStart(atStart), m_atEnd(atEnd) { }

private:
    bool m_atStart;
    bool m_atEnd;
};

class BeamedGroupInfoImpl : virtual public GroupInfoImpl,
			    virtual public BeamedGroupInfo_skel
{
public:
    BeamedGroupInfoImpl(bool atStart, bool atEnd) :
	GroupInfoImpl(atStart, atEnd) { }
    virtual ~BeamedGroupInfoImpl() { }

    virtual void visit(GroupInfoVisitor_ptr giv) {
	BeamedGroupInfo_var bgi(_this());
	giv->visitBeamedGroup(bgi);
    }

    // resolve multiple inheritance ambiguity
    BeamedGroupInfo_ptr _this() { return BeamedGroupInfo_skel::_this(); }
};

class TupledGroupInfoImpl : virtual public GroupInfoImpl,
			    virtual public TupledGroupInfo_skel
{
public:
    TupledGroupInfoImpl(bool atStart, bool atEnd, Time normalLength,
			Time tupledLength, short tupledCount)
	: GroupInfoImpl(atStart, atEnd), m_normalLength(normalLength),
	  m_tupledLength(tupledLength), m_tupledCount(tupledCount) { }
    virtual ~TupledGroupInfoImpl() { }

    virtual Time getNormalLength() { return m_normalLength; }
    virtual Time getTupledLength() { return m_tupledLength; }
    virtual CORBA::Short getTupledCount() { return m_tupledCount; }

    virtual void visit(GroupInfoVisitor_ptr giv) {
	TupledGroupInfo_var bgi(_this());
	giv->visitTupledGroup(bgi);
    }
   
    // resolve multiple inheritance ambiguity
    TupledGroupInfo_ptr _this() { return TupledGroupInfo_skel::_this(); }

private:
    Time m_normalLength;
    Time m_tupledLength;
    CORBA::Short m_tupledCount;
};

// pretty much untested yet -- need to check refcounts on GroupInfo
// objects; do they get freed when their owning Element does?

class GroupInfoFactoryImpl : virtual public GroupInfoFactory_skel,
			     virtual public Transactor
{
public:
    GroupInfoFactoryImpl(Transactable *t) : Transactor(t) { }
    virtual ~GroupInfoFactoryImpl() { }

    virtual void setBeamedGroupInfo
    (TransactionId id, Element_ptr e,
     CORBA::Boolean atStart, CORBA::Boolean atEnd)
	throw (BadTransaction);

    virtual void setTupledGroupInfo
    (TransactionId id, Element_ptr e,
     CORBA::Boolean atStart, CORBA::Boolean atEnd,
     Time normalLength, Time tupledLength, CORBA::Short tupledCount)
	throw (BadTransaction);
};

#endif

