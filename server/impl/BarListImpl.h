// -*- c-file-style:  "bsd" -*-
#ifndef _BAR_LIST_IMPL_H_
#define _BAR_LIST_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include <common/RobustIteratorList.h>
#include <vector>

using namespace Rosegarden;
using namespace Rosegarden::Server;
class CompositionImpl;

// We can keep a check on the Time of the bar, and get updates from
// the Part when something is updated; then each Bar could get its
// iterator to update to that Bar's correct time, if possible.

// Fixed bars?  Something in the Part that indicates a new bar.  With
// a time signature.  The Bar's iterator, once pointing to that item,
// should remain pointed at it forever (if we trust the
// RobustIteratorList... which we must) unless it's deleted -- so
// before updating the iterator from the Time, we should check whether
// the iterator points at a NewBar object and if so obey it.  Cue
// problems if we insert into the last flexible Bar before a NewBar,
// and have to create a new Bar object in between to take up the
// slack.

// Need to keep a handle on whether each bar is of the correct length;
// incorrect-length bars tolerated but should be marked up as such in
// the score?

// We no longer split notes automatically across bar lines.  That was
// cute but added nothing.

//!!! This code is incomplete, but I think the structures are broadly
// right. i.e. a BarList should contain a list of BarArrays each of
// which is a set of Bars on individual Parts.  The point is to make
// accessing all Parts at once at a given (graphical) point fairly
// straightforward.  This is pretty similar to rg2.1 in intention

class BarListImpl : virtual public BarList_skel
{
public:
    struct Bar // represents Bar n (for some n) on part m (for some m)
    {
	// endTime is not necessarily startTime + bar-length of time sig;
	// the bar might be the wrong length if the notes don't fit exactly
	Time startTime;
	Time endTime;
	TimeSignature timeSignature;
	PartTimedIterator_ptr iterator;
    };

    struct Validity
    {
	Part_ptr part;
	long invalidFrom; // index of first element changed; -1 if no change
    };

    // to find a bar in a Part, call indexOfPart and use the result on
    // the relevant BarArray

    typedef vector<Bar> BarArray; // Bar n (for some n), all parts
    typedef RobustIteratorList<BarArray> BarArrayList; // Bars 1 - n, all parts
    typedef BarArrayList::iterator iterator;
    typedef vector<Validity> BarValidityArray; // Info about parts

public: // IDL interface

    BarListImpl(CompositionImpl *composition);
    virtual ~BarListImpl();

    virtual CORBA::Short getBarCount();

    virtual BarListIterator_ptr getIterator();
    virtual BarListIterator_ptr getIteratorAtEnd();
    virtual BarListIterator_ptr getContainingBar(PartBasicIterator_ptr pbi,
						 CORBA::Short &partIndex)
	throw (NoSuchPart);

public: // but for library use only (for the iterator, and Part/Compo cb)

    iterator begin() { return m_bars.begin(); }
    iterator end() { return m_bars.end(); }

    PartTimedIterator_ptr getPartTimedIterator(Part_ptr part, iterator posn)
	throw (NoSuchPart);

    virtual void justInsertedPartCallback(Composition_ptr, CORBA::Short index, Part_ptr);
    virtual void aboutToErasePartCallback(Composition_ptr, CORBA::Short index, Part_ptr);
    virtual void justInsertedElementCallback(Part_ptr, PartBasicIterator_ptr);
    virtual void aboutToEraseElementCallback(Part_ptr, PartBasicIterator_ptr);
    virtual void justOverlaidElementCallback(Part_ptr, PartBasicIterator_ptr);
    virtual void aboutToStripElementCallback(Part_ptr, PartBasicIterator_ptr);
//    virtual void justRepositionedElementCallback(Part_ptr, PartBasicIterator_ptr);

    void reformat();

private:
    CompositionImpl *m_composition;
    BarArrayList m_bars;
    BarValidityArray m_validity; // must have same number of elts as each elt of m_bars has
    bool m_somethingChanged; // but you have to check m_validity to see what

    void appendNewBar();
    void addPart(short index, Part_ptr part);
    void removePart(short index);
    short indexOfPart(Part_ptr part);
    void setEndTimeOfPrevious(BarArrayList::iterator &, short partIndex);
};

class BarListIteratorImpl : virtual public BarListIterator_skel
{
public:
    BarListIteratorImpl(BarListImpl *, const BarListImpl::iterator &itr);
    virtual ~BarListIteratorImpl();

    virtual void releaseImpl();
    virtual BarListIterator_ptr clone();

    virtual void goTo(BarListIterator_ptr bli)
	throw (IncompatibleIterators);

    virtual CORBA::Boolean atLeftEnd();
    virtual CORBA::Boolean atRightEnd();

    virtual CORBA::Boolean goLeft();
    virtual CORBA::Boolean goRight();

    virtual CORBA::Short getBarNumber();
    virtual TimeSignature getTimeSignature(Part_ptr part)
	throw (NoSuchPart);
    virtual Time getBarDuration(Part_ptr part)
	throw (NoSuchPart);
    virtual Time getMaxBarDuration(Part_ptr &part);

    virtual PartBasicIterator_ptr getPartIterator(Part_ptr part)
	throw (NoSuchPart);

private:
    BarListImpl *m_barListImpl;
    BarListImpl::iterator m_iterator;
};

#endif

