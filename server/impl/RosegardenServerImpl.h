// -*- c-file-style:  "bsd" -*-
#ifndef _ROSEGARDEN_SERVER_IMPL_H_
#define _ROSEGARDEN_SERVER_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include <common/my_wstring.h>
#include <string>
#include <vector>

extern void new_summarise();

using namespace Rosegarden;
using namespace Rosegarden::Server;
class CompositionImpl;

class RosegardenServerImpl : virtual public Server_skel
{
public:
    RosegardenServerImpl() { }
    virtual ~RosegardenServerImpl();

    virtual short getCompositionCount();
    virtual CompositionIdList *getCompositionIdList();
    virtual Composition_ptr newComposition();
    virtual CORBA::WChar *getCompositionName(CompositionId id)
	throw (NoSuchComposition);
    virtual Composition_ptr openComposition(CompositionId id)
	throw (NoSuchComposition);
    virtual void closeComposition(Composition_ptr c)
	throw (NoSuchComposition);

    virtual void registerPermanentImportService(ImportService_ptr i);
    virtual void registerPermanentExportService(ExportService_ptr e);

    virtual void registerImportService
    (const char *objectName, const CORBA::WChar *typeName, const char *extensions);
    virtual void registerExportService
    (const char *objectName, const CORBA::WChar *typeName, const char *extensions, const char *defaultExtension);

    virtual void getImportFileTypes
    (WStringList*& names, StringList*& extensions);
    virtual void getExportFileTypes
    (WStringList*& names, StringList*& extensions, StringList*& defaultExtensions);

    virtual Composition_ptr loadComposition(const char *name)
        throw (IOServiceFailed);

    virtual void saveComposition(const char *name, Composition_ptr c)
        throw (IOServiceFailed);

    virtual Composition_ptr importComposition
    (const CORBA::WChar *type, const char *name)
        throw (UnknownFileType, IOServiceFailed);

    virtual void exportComposition
    (const CORBA::WChar *type, const char *name, Composition_ptr c)
        throw (UnknownFileType, IOServiceFailed);

    virtual Composition_ptr importCompositionByExtension(const char *name)
        throw (UnclassableExtension, IOServiceFailed);

    virtual void exportCompositionByExtension(const char *name, Composition_ptr c)
        throw (UnclassableExtension, IOServiceFailed);

    virtual void dump();
    virtual void quit();

private:
    struct ServiceRec {
	union {
	    ImportService_ptr importService;
	    ExportService_ptr exportService;
	};
	string objectName;
	my_wstring typeName;
	string extensions; // regexp
	string defaultExtension; // for export only
    };

    vector<ServiceRec> m_importServices;
    vector<ServiceRec> m_exportServices;
    void installService(vector<ServiceRec> &, bool import, ServiceRec s);
    string extensionOfFile(string fileName);

    struct CompositionRec {
	CompositionImpl *composition;
	CompositionId id;
	short refCount;
    };

    vector<CompositionRec> m_compositions;
    static CompositionId m_nextCompositionId;

    vector<CompositionRec>::iterator locate(Composition_ptr c)
        throw (NoSuchComposition);
    vector<CompositionRec>::iterator locate(CompositionId id)
        throw (NoSuchComposition);

    CORBA::Object_ptr bind(string name);
};

#endif
