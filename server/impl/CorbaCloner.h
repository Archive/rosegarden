// -*- c-file-style:  "bsd" -*-
#ifndef _CORBA_CLONER_H_
#define _CORBA_CLONER_H_

#include <common/CloningWrapper.h>
#include <CORBA.h>

// Cloner for basic CORBA types; because it uses T::_duplicate etc.,
// it can't be used for heterogeneous lists (for which the duplicate
// method needs to be virtual, called through the base-class).  But
// it does allow us to have lists of interface pointers without
// having to worry about (de)allocation.

template <class T, class Tptr>
struct CorbaCloner
{
    Tptr null() const { return T::_nil(); }
    Tptr clone(const Tptr t) const { return T::_duplicate(t); }
    void release(Tptr t) const { CORBA::release(t); }
};

// Heterogeneous lists of ...Impl objects are easier, and needn't
// involve the CorbaCloner at all

#endif

