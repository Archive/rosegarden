// -*- c-file-style:  "bsd" -*-
#ifndef _PART_ITERATOR_BODY_H_
#define _PART_ITERATOR_BODY_H_

#include <server/idl/RosegardenServer.h>
#include "PartImpl.h"

#include <iterator>

using namespace Rosegarden;
using namespace Rosegarden::Server;

// Concrete.  Do not derive from this -- it'd be too complicated to
// change some behaviour and still get all the possible cases right

class PartIteratorBody
{
public:
    PartIteratorBody(PartImpl *pi, const PartImpl::iterator &itr, bool notesOnly);
    explicit PartIteratorBody(const PartIteratorBody &b);
    ~PartIteratorBody() {
	if (m_partImpl) m_partImpl->unregisterIteratorBody(this);
	delete m_iterator;
    }


    // ITERATOR methods

    long getIndex() {
	if (!isOK()) return 0;
	return distance(m_partImpl->begin(), *m_iterator);
    }
    Part_ptr getPart() {
	if (!isOK()) return Part::_nil();
	return Part::_duplicate(m_partImpl);
    }
    
    CORBA::Boolean samePartAs(PartBasicIterator_ptr i) {
	if (!isOK()) return false;
	Part_ptr pp(i->getPart());
	CORBA::Boolean equal(m_partImpl->_is_equivalent(pp));
	CORBA::release(pp);
	return equal;
    }

    CORBA::Boolean isBefore(PartBasicIterator_ptr i);
    CORBA::Boolean isSameAs(PartBasicIterator_ptr i);

    CORBA::Boolean isOK() {
	if (m_iterator == 0) return false;
	else return true;
    }

    void goTo(PartBasicIterator_ptr i) throw (IncompatibleIterators);

    CORBA::Boolean atLeftEnd() {
	if (!isOK()) return true;
	if (*m_iterator == m_partImpl->begin()) return (m_chordNoteIndex <= 0);
	else return false;
    }

    CORBA::Boolean atRightEnd() {
	if (!isOK()) return true;
	// This should be correct even for note-iterators with chord indices
	return (*m_iterator == m_partImpl->end());
    }

    // These are the basic motion methods; if any other methods want
    // to change the position of the iterator they should either call
    // these or else carefully document the fact that they don't.
    CORBA::Boolean goLeft();
    CORBA::Boolean goRight();

    Element_ptr getLeftElement();
    Element_ptr getRightElement();

    // must be implemented to use getLeftElement / getRightElement -->
    Element_ptr nextLeft() {
	if (!isOK()) return Element::_nil();
	if (!atLeftEnd()) { goLeft(); return getRightElement(); }
	else return Element::_nil();
    }

    Element_ptr nextRight() {
	if (!isOK()) return Element::_nil();
	if (!atRightEnd()) { Element_ptr e(getRightElement()); goRight(); return e; }
	else return Element::_nil();
    }

    CORBA::Boolean getLeftElementLocked(Element_ptr &e);
    CORBA::Boolean getRightElementLocked(Element_ptr &e);

    CORBA::Boolean nextLeftLocked(Element_ptr &e);
    CORBA::Boolean nextRightLocked(Element_ptr &e);


    // MUTATOR methods

    // insert is most unlikely to insert something in the middle of a
    // chord, but I guess deleteLeft() and deleteRight() should be
    // allowed to delete single notes from chords...
    void insert(Element_ptr e);
    CORBA::Boolean deleteLeft();
    CORBA::Boolean deleteRight();
    CORBA::Boolean deleteLeftLocked(CORBA::Boolean &d);
    CORBA::Boolean deleteRightLocked(CORBA::Boolean &d);

    
    // TIMED ITERATOR methods

    // won't land in the middle of a chord:
    void goToTime(Time t) throw (InvalidTime);
    Time getTimeFromStart();
    Time getTimeFromEnd();
    CORBA::Short getChordNoteIndex() {
	return m_chordNoteIndex; // even if !isOK(), we don't care
    }


    // TIMED MUTATOR methods

    void overlay(Element_ptr e);
    void overlayAtTime(Element_ptr e, Time t);

    CORBA::Boolean stripLeft();
    CORBA::Boolean stripRight();
    CORBA::Boolean stripLeftLocked(CORBA::Boolean &d);
    CORBA::Boolean stripRightLocked(CORBA::Boolean &d);


    // For call from other impl classes

    void partDeletedCallback();
    void aboutToEraseElementCallback(const PartImpl::iterator &i);
    void justInsertedElementCallback(const PartImpl::iterator &i);
    void aboutToSetGapTimeCallback(const PartImpl::iterator &i, Time t);

    const PartImpl::iterator &iterator() const {
	assert(((PartIteratorBody *)this)->isOK());
	return *m_iterator;
    }

private:
    PartImpl *m_partImpl;

    // m_iterator is supposed to be somewhat opaque.  We shouldn't
    // really do *anything* with it except pass it to PartImpl
    // functions, assign to it, increment and decrement, and compare
    // with PartImpl::begin() and end().
    // The only reason this is a pointer is because we want 0 to
    // mean the Part has been deleted without us
    PartImpl::iterator *m_iterator;

    // This is only used if notesOnly is true, in which case we never
    // return a Chord from any of the accessor functions but instead
    // return individual notes from that Chord.
    short m_chordNoteIndex;	// -1 == whole chord, 0->n-1 == individual note

    bool notesOnly() { return m_chordNoteIndex >= 0; }
    short calculateChordNoteCount();
    Chord_ptr currentChord();	// this is duplicated, caller must release

//    void goToBody(const PartIteratorBody &b)
//	throw (IncompatibleIterators);

    bool checkInternalTime() { // returns true if time had to be reset
	if (!m_internalTimeSet && isOK()) {
	    m_internalTime = m_partImpl->getTimeFromStart(*m_iterator);
	    m_internalTimeSet = true;
	    return true;
	} else return false;
    }

    Time getInternalTime() const {
	((PartIteratorBody *)this)->checkInternalTime();
	return m_internalTime;
    }

    void setInternalTime(Time t) {
	m_internalTime = t;
#ifdef DEBUG
	cout << "PartIteratorBody " << this << ": internal time set to "
	     << m_internalTime << endl;
#endif
    }
    void adjustInternalTime(Time t) {
	// if you're calling this from a callback, be careful -- call
	// checkInternalTime first, and if it returns true, you might
	// not want to adjust at all because the internal time might
	// now be correct
	((PartIteratorBody *)this)->checkInternalTime();
	m_internalTime += t;
#ifdef DEBUG
	cout << "PartIteratorBody " << this << ": internal time adjusted by "
	     << t << " to " << m_internalTime << endl;
#endif
    }

    // points to the time at which the next note starts:
    // i.e. if we're at the beginning of the piece, this time
    // will be the first note's gapTime, not necessarily zero
    Time m_internalTime;
    bool m_internalTimeSet;

    PartIteratorBody &operator=(const PartIteratorBody &); // ditto
};


#endif

