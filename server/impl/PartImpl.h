// -*- c-file-style:  "bsd" -*-
#ifndef _PART_IMPL_H_
#define _PART_IMPL_H_

#include <server/idl/RosegardenServer.h>
#include <common/RobustIteratorList.h>
#include <common/FastInsertVector.h>
#include <common/CloningWrapper.h>
#include <common/Lockable.h>

#include "ElementImpl.h"
#include "Transaction.h"

using namespace Rosegarden;
using namespace Rosegarden::Server;

// Any code that changes the list in any way that might affect times
// (durations of notes, gapTimes, insert/delete etc) *must* be very
// careful about the effects on TimedIterators and m_totalDuration.
// It'll have to make sure all the notifications are done *itself*.

// PartImpl, PartIteratorImpl and PartIteratorBody are fairly closely
// tied together.  They exchange quite a lot of information other than
// through the public APIs.  Other classes should not be so bold.

class PartIteratorBody;
class CompositionImpl;
class BarListImpl;

class ElementWrapper
{
public:
    ElementWrapper(Time t = 0);
    ElementWrapper(const Element_ptr e, Time t = 0);
    ElementWrapper(const ElementWrapper &ew);
    ElementWrapper &operator=(const ElementWrapper &ew);
    ~ElementWrapper();

    Time getGapTime() const { return m_time; }
    void setGapTime(Time t) { m_time = t; }

    Element_ptr data() const;
    Element_ptr cloned_data() const;

private:
    Time m_time;
    Element_ptr m_element;
};

ostream &operator<<(ostream &o, const ElementWrapper *e);

// A PartImpl is locked when an iterator is created, and unlocked when
// all iterators have been released (i.e. an iterator makes an unlock
// request when it is released, but the request is ignored unless it's
// the last iterator).  Locks also timeout, so we're not completely
// shafted if a client fails to release an iterator: it'll just risk
// making deleting a Part abysmally slow.

class PartImpl : virtual public Part_skel,
		 virtual public Transactor
{
    typedef RobustIteratorList<ElementWrapper,
	                       FastInsertVector<ElementWrapper> >
        ElementPtrList;

public:
    PartImpl(CompositionImpl *, Transactable *);
    virtual ~PartImpl();

    // public (CORBA) interface

    virtual PartBasicIterator_ptr getBasicIterator();
    virtual PartBasicIterator_ptr getBasicIteratorAtEnd();
    virtual PartBasicMutator_ptr getBasicMutator();
    virtual PartBasicMutator_ptr getBasicMutatorAtEnd();

    virtual PartTimedIterator_ptr getTimedIterator();
    virtual PartTimedIterator_ptr getTimedIteratorAtEnd();
    virtual PartTimedMutator_ptr getTimedMutator();
    virtual PartTimedMutator_ptr getTimedMutatorAtEnd();

    virtual PartTimedNoteIterator_ptr getTimedNoteIterator();
    virtual PartTimedNoteIterator_ptr getTimedNoteIteratorAtEnd();
    virtual PartTimedNoteMutator_ptr getTimedNoteMutator();
    virtual PartTimedNoteMutator_ptr getTimedNoteMutatorAtEnd();

    virtual CORBA::Long getElementCount();
    virtual Time getTotalDuration() { return m_duration; }
    virtual void padWithRestsTo(Time totalDuration)
	throw (InvalidTime);

    virtual void registerObserver(PartObserver_ptr);
    virtual void unregisterObserver(PartObserver_ptr);

    virtual void dump(); // for debugging

    // library interface, for use only by IteratorImpl classes.
    // any functions in here that change the timing of elements
    // (insert, erase, setGapTime) must make the correct
    // callbacks on any affected TimedIterator objects, and
    // ensure that m_duration remains correct

    typedef ElementPtrList::iterator iterator;

    virtual iterator begin() const { return m_elements.begin(); }
    virtual iterator end() const   { return m_elements.end();   }

    virtual iterator insert(const iterator &i, Element_ptr e, bool callCallbacks);
    virtual void erase(iterator &i, bool callCallbacks);

    virtual iterator getIteratorAtTime(Time t) const;

    virtual Time getTimeFromStart(const iterator &i) const;
    virtual Time getTimeFromEnd(const iterator &i) const;

    virtual Time getGapTime(const iterator &i) const { return i->getGapTime(); }
    virtual void setGapTime(iterator &i, Time t);
  
    virtual bool hasSomeLockedElements(); // may be slow

    // The PartImpl::iterator member of a PartIterator is registered
    // with the RobustIteratorList, so it moves around correctly when
    // the list is changed.  But IteratorBodies need to get separate
    // notifications, so they can maintain their own timing data.
    virtual void registerIteratorBody(PartIteratorBody *);
    virtual void unregisterIteratorBody(PartIteratorBody *);

    virtual void callJustInsertedElementCallbacks(const iterator &);
    virtual void callJustOverlaidElementCallbacks(const iterator &);
    virtual void callAboutToEraseElementCallbacks(const iterator &);
    virtual void callAboutToStripElementCallbacks(const iterator &);

private:
    CompositionImpl *m_composition;
    ElementPtrList m_elements;
    vector<PartIteratorBody *> m_iterators;
    vector<PartObserver_ptr> m_observers;
    Time m_duration;

    void callJustInsertedOnIteratorBodies(const iterator &);
    void callAboutToEraseOnIteratorBodies(const iterator &);
    void callAboutToSetGapTimeOnIteratorBodies(const iterator &, Time);
    void callAboutToDeletePartOnIteratorBodies();
};

#endif

