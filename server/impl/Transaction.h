// -*- c-file-style:  "bsd" -*-
#ifndef _TRANSACTION_H_
#define _TRANSACTION_H_

#include <server/idl/RosegardenServer.h>
#include <common/my_wstring.h>
#include <common/Lockable.h>

class Transactable : private TransactionLockable<10000>
// classes that want things done to them derive from this
// (probably only Composition, in the Rosegarden world)
{
public:
    Transactable() { }
    virtual ~Transactable() { }

    bool transactionIsGood(Rosegarden::Server::TransactionId id);

    // false -> lock failed
    bool lockTransaction(my_wstring name, Rosegarden::Server::TransactionId &id);
    void unlockTransaction(Rosegarden::Server::TransactionId id);

private:
    my_wstring m_name;
};

class Transactor
// classes that want to do things derive from this
{
public:
    Transactor(Transactable *t) : m_transactable(t) { }

    Transactable *getTransactable() const { return m_transactable; }

    virtual void checkGood(Rosegarden::Server::TransactionId id)
	throw (Rosegarden::Server::BadTransaction);

private:
    Transactable *m_transactable;
};

#endif

