// -*- c-file-style:  "bsd" -*-
#ifndef _STATUS_WRAPPER_H_
#define _STATUS_WRAPPER_H_

#include <utilities/idl/RosegardenCommon.h>
#include <algorithm>
#include <string>

#include <common/my_wstring.h>

using namespace Rosegarden;

class StatusWrapper
{
public:
    StatusWrapper(my_wstring s = "") : m_text(s) { }
    ~StatusWrapper() { }

    Status *status() {
	Status *s = new Status;
	s->succeeded = (m_text.size() == 0);
	s->errorText = CORBA::wstring_dup(m_text.c_str());
	return s;
    }

private:
    my_wstring m_text;
};

#ifdef NOT_DEFINED

class StatusWrapper
{
    //!!! bleah -- seems the egcs STL implementation doesn't
    // implement basic_string<wchar_t> -- no specialisation
    // for basic_string<wchar_t>::Rep and its methods
    typedef vector<wchar_t> string_type;

public:
    StatusWrapper(string s);
    StatusWrapper(string_type s = string_type()) :
	m_text(s) { }
    ~StatusWrapper() { }

    Status status() {

	int i;
	static wchar_t text[100];
	for (i = 0; i < 99 && i < m_text.size(); ++i) {
	    text[i] = m_text[i];
	}
	text[i] = 0;	

	Status s;
	s.succeeded = (m_text.size() == 0);
	s.errorText = CORBA::wstring_dup(text);
	return s;
    }

private:
    string_type m_text;
    friend struct AppendTo { // gosh, is that legal?
	AppendTo(StatusWrapper *s) : m_s(s) { }
	void operator()(const char &c) { m_s->m_text.//append(1, (wchar_t)c); }
					     push_back((wchar_t)c); }
	StatusWrapper *m_s;
    };
};

#endif

#endif
